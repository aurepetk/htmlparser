﻿using System;
using HtmlParser;
using HtmlParser.Storage;
using HtmlParser.View;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleView.PrintInstructions();
            var storage = new DbStorage();
            var driverFactory = new WebDriverFactory();
            var driver = driverFactory.BuildDriver();
        }
    }
}
