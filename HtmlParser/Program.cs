﻿using System;
using HtmlParser.MainController;
using HtmlParser.View;

namespace HtmlParser
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleView.PrintInstructions();
            var valid = int.TryParse(Console.ReadLine(), out int choice);

            while (valid)
            {
                var operationFactory = new OperationExecutor();
                operationFactory.BuildOperation(choice);

                ConsoleView.AskForAnotherInput();
                valid = int.TryParse(Console.ReadLine(), out choice);
            }
        }
    }
}
