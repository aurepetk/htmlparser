﻿using System;
using System.Collections.Generic;
using HtmlParser.Entities;

namespace HtmlParser.View
{
    public static class ConsoleView
    {
        public static void PrintInstructions()
        {
            Console.WriteLine("Please choose one of the following:");
            Console.WriteLine("0 - Delete profiles from database.");
            Console.WriteLine("00 - Delete addresses from database.");
            Console.WriteLine("1 - Read webpages and import data from database to csv.");
            Console.WriteLine("2 - Only import profiles from database to csv.");
            Console.WriteLine("3 - Read softguide.de webpage and import data from database to csv.");
            Console.WriteLine("4 - Read softwaresearch.com webpage and import data from database to csv.");
            Console.WriteLine("5 - Fill country postal codes table.");
            Console.WriteLine("6 - Fix addresses.");
            Console.WriteLine("7 - Delete country codes.");
            Console.WriteLine("8 - Import extra profile info to CSV.");
            Console.WriteLine("9 - Read softwaregids.nl webpage and import data from database to csv.");
        }

        public static void AskForAnotherInput()
        {
            Console.WriteLine("Please insert another choice or press any other key than number to exit.");
        }

        public static void WaitForInputToContinue()
        {
            Console.WriteLine("Press any key to see results..");
            Console.ReadKey();
        }

        public static void DisplayParsedEntities(List<Profile> profiles)
        {
            Console.WriteLine("-----Contact info-----");
            foreach (var profile in profiles)
            {
                Console.WriteLine("--Profile--");
                var entityInfo = string.Format(" {0,15}:{1} ",
                    "Company", profile.CompanyName);
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "Contact Person", profile.ContactPerson);
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "Email", profile.Email);
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "WebPage", profile.WebPage);
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "Phone", profile.Phone);
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "Address", profile.GetAddressLine());
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "Description", profile.Description);
                Console.WriteLine(entityInfo);

                entityInfo = string.Format(" {0,15}:{1} ",
                    "Gender", profile.Gender.ToString());
                Console.WriteLine(entityInfo);
            }
            Console.WriteLine("--Profile--");
            Console.WriteLine("-----Contact info-----");
        }

    }
}
