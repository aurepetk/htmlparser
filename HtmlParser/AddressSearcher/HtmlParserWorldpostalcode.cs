﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Debugging;
using Google.Cloud.Translation.V2;
using HtmlParser;
using HtmlParser.ConfigurationHelper;
using HtmlParser.Entities; 
using HtmlParser.Storage;
using HtmlParseTools.AnonymousBrowsing;
using HtmlParseTools.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace AddressSearcher
{
    public class HtmlParserWorldpostalcode
    {
        private IWebDriver driver;
        private string source = @"https://worldpostalcode.com/search/";
        private DbStorage storage;
        private WebDriverFactory driverFactory;
        private string selectedDriver;

        public HtmlParserWorldpostalcode()
        {
            SetDefaults();
            driver = driverFactory.BuildDriver(selectedDriver);
        }

        public void FixAddresses()
        {
            var addressesToBeFixed = GetUpdatableAddresses(true);
            storage.UpdateAddresses(addressesToBeFixed);
            Console.WriteLine($"Fixed {addressesToBeFixed.Count} addresses");
        }

        private List<Address> GetUpdatableAddresses(bool includeFixed = false)
        {
            
            var toBeFixedAddresses = storage.GetAddresses(includeFixed);
            var addressFixes = new List<Address>();
            var countryCodesMap = storage.GetCountryPostalCodesMap();
            var reverseCountryCodesMap = ReverseCountryMap(countryCodesMap);
            var translatedCountries = new Dictionary<string, string>();

            foreach (var address in toBeFixedAddresses)
            {
                try
                {
                    var foundAddress = Search(address.PostalCode, address.CountryCode);
                    if (foundAddress == null)
                    {
                        address.Flag(FixedStatus.Failed);

                        addressFixes.Add(address);
                        Logger.Log($"FAILED:{address.PostalCode + " " + address.CountryCode}");
                        break;
                    }
                    string translatedCountry;
                    if (!translatedCountries.ContainsKey(foundAddress.Country))
                    {
                        translatedCountry = TranslateToEnglish(foundAddress.Country);
                        translatedCountries.Add(foundAddress.Country, translatedCountry);
                    }
                    else
                    {
                        translatedCountry = translatedCountries[foundAddress.Country];
                    }

                    if (!reverseCountryCodesMap.ContainsKey(translatedCountry))
                    {
                        Logger.Log($"FAILED:{foundAddress.PostalCode + " " + foundAddress.CountryCode}");
                        continue;
                    }

                    address.CountryCode = reverseCountryCodesMap[translatedCountry];

                    foundAddress.Country = translatedCountry;
                    if (reverseCountryCodesMap.ContainsKey(foundAddress.Country))
                    {
                        address.CountryCode = reverseCountryCodesMap[foundAddress.Country];
                    }

                    address.Country = foundAddress.Country;
                    address.PostalCode = foundAddress.PostalCode + " " + foundAddress.CountryCode;
                    address.Flag(FixedStatus.Fixed);
                    Console.WriteLine(address.Print());
                    Logger.Log(address.Print());
                    addressFixes.Add(address);

                }
                catch (Exception)
                {
                    // ignored
                }
            }

            return addressFixes;
        }

        private string TranslateToEnglish(string text)
        {
            try
            {
                if (driver.IsMultipleTabs())
                {
                    driver.NavigateToTabOfIndex(1);
                }
                else
                {
                    driver.OpenNewTab();

                    driver.NavigateToTabOfIndex(1);
                    driver.Url = "https://translate.google.com/";
                }

                var translatorElementXpath = "//*[@id=\"source\"]";
                var translatorElement = driver.WaitUntilElementExists(By.XPath(translatorElementXpath));

                var translationElementXpath = "//*[@id=\"gt-res-dir-ctr\"]";
                var translationElement = driver.WaitUntilElementExists(By.XPath(translationElementXpath));

                var englishLanguageXpath = "//*[@id=\"sugg-item-en\"]";
                var englishLanguageButton = driver.WaitUntilElementExists(By.XPath(englishLanguageXpath));

                var detectLanguageButtonXpath =
                    "/html/body/div[3]/div[2]/form/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/div/div[5]";
                var detectLanguageButton = driver.WaitUntilElementExists(By.XPath(detectLanguageButtonXpath));

                englishLanguageButton.Click();
                detectLanguageButton.Click();

                translatorElement.Clear();
                translatorElement.SendKeys(text);

                Thread.Sleep(2000);

                var translation = translationElement.Text;
                translation = translation.Replace("The", "").Trim();

                driver.NavigateToTabOfIndex(0);
                return translation;
            }
            catch (Exception)
            {
                return text;
            }
        }

        private Dictionary<string, string> ReverseCountryMap(Dictionary<string, string> countryMap)
        {
            var reverseMap = new Dictionary<string, string>();
            foreach (var countryMapPair in countryMap)
            {
                reverseMap.Add(countryMapPair.Value, countryMapPair.Key);
            }

            return reverseMap;
        }

        private Address Search(string postalCode, string countryCode, int attempts = 0)
        {
            NavigateToSource();
            if (((FirefoxDriver) driver).CheckForCaptcha())
            {
                TorDriver.CallNewTorSession();
                Thread.Sleep(5000);
                NavigateToSource();
            }
            var inputField = GetInputField();

            inputField.Clear();
            inputField.SendKeys(postalCode + countryCode);
            SubmitSearch();
            WaitTillLoaded();

            var country = GetCountry();
            if (string.IsNullOrEmpty(country)) return null;
            if (country.ToLower().Contains("could not find"))
            {
                TorDriver.CallNewTorSession();

                if (attempts > 0) return null;
                attempts++;
                Search(postalCode, countryCode, attempts);
            }
            var addressUpdate = new Address(countryCode, country, postalCode);
            return addressUpdate;
        }

        public void WaitTillLoaded()
        {
            ImitateWaiting();
            //string currentElementId = oldElementID;
            //while (string.CompareOrdinal(oldElementID, currentElementId) == 0)
            //{
            //    var inputField = GetInputField();

            //}
        }

        public void ImitateWaiting()
        {
            var random = new Random();
            var waitTime = random.Next(1000, 2000);
            Thread.Sleep(waitTime);
        }

        public HtmlParserWorldpostalcode(IWebDriver driver)
        {
            SetDefaults();
            this.driver = driver;
        }

        public void SetDefaults()
        {
            selectedDriver = ConfigurationStore.Instance.SelectedDriver;
            storage = new DbStorage();
            driverFactory = new WebDriverFactory();
        }

    private void NavigateToSource()
        {
            if(driver.Url != source)
                driver.TryNavigate(source);
        }

        private IWebElement GetInputField()
        {
            var inputFieldXpath = @"/html/body/div[1]/div[1]/div/div/div[2]/form/input[2]";
            return driver.WaitUntilElementClickable(By.XPath(inputFieldXpath));
        }

        private void SubmitSearch()
        {
            var searchButton = GetSubmitButtonElement();
            driver.JavaScriptClick(searchButton);
        }

        private IWebElement GetSubmitButtonElement()
        {
            var xPath = @"/html/body/div[1]/div[1]/div/div/div[2]/form/input[1]";
            return driver.FindElement(By.XPath(xPath));
        }

        private string GetCountry()
        {
            try
            {
                var addressBox = GetAddressBox();
                var addressText = addressBox.Text;

                var addressLines = addressText.Split(new string[] { "\r\n" },
                    StringSplitOptions.None);
                var countryLine = addressLines[2];
                var countryLineParts = countryLine.Split(',');
                var country = countryLineParts[countryLineParts.Length - 1];
                return country;
            }
            catch (Exception)
            {
                return "";
            }

        }

        private string GetPostalCode()
        {
            var addressBox = GetAddressBox();
            var addressText = addressBox.Text;

            var addressLines = addressText.Split(new string[] { "\r\n" },
                StringSplitOptions.None);
            var postalCodeLine = addressLines[0];
            return postalCodeLine;
        }

        private IWebElement GetAddressBox()
        {
            var xPath = @"/html/body/div[2]/div/div[1]/div[2]/div[1]/div[6]/div/div[1]/div";
            return driver.WaitUntilElementVisible(By.XPath(xPath));
        }

    }
}
