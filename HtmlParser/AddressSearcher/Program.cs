﻿using System;
using System.Net.Mime;
using HtmlParser;
using HtmlParser.ConfigurationHelper;
using HtmlParseTools.AnonymousBrowsing;
using HtmlParseTools.Extensions;

namespace AddressSearcher
{
    class Program
    {
        static void Main(string[] args)
        {
            var addressSearcher = new HtmlParserWorldpostalcode();
            addressSearcher.FixAddresses();
            TorDriver.CleanUp();
            Environment.Exit(0);
        }
    }
}
