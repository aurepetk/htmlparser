﻿namespace HtmlParser.Storage
{
    /*
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }

        public string Branch { get; set; }
        public string Certificates { get; set; }
        public string MotherCompany { get; set; }
        public string DaughterCompanies { get; set; }
        public string NumberOfOffices { get; set; }
        public string NumberOfEmployes { get; set; }
        public string ExtraServices { get; set; }
        public string CountryOfOrigin { get; set; }
        public string TargetGroup { get; set; }
     */
    public enum ProfileColumns
    {
        Company, Fullname, Id, Email, Phone, Gender, Address, Description, WebPage, Branch, Certificates, MotherCompany,
        DaughterCompanies, NumberOfOffices, NumberOfEmployees, ExtraServices, CountryOfOrigin, TargetGroup, Source, addressId
    }
    public enum AddressColumns { Id, Street, CountryCode, City, Country, Guid, PostalCode, IsFixed }
    public enum CountryPostalCodeColumns { Id, Country, CountryCode }
}