﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using HtmlParser.Entities;

namespace HtmlParser.Storage
{
    public class DbStorage
    {
        private readonly SqlManager _sqlManager;

        public DbStorage()
        {
            _sqlManager = new SqlManager();
        }
        public DbStorage(string pathToDb)
        {
            _sqlManager = new SqlManager(pathToDb);
        }

        #region Profiles

        public List<Profile> GetProfiles()
        {
            var profilesQuery = new ProfilesQuery();
            var cmd = profilesQuery.BuildCommand(DbOperation.Select);
            var profilesTable = _sqlManager.GetTable(cmd);
            var profiles = Profile.ConvertFromTable(profilesTable);
            return profiles;
        }

        public void CreateProfiles(List<Profile> profiles)
        {
            foreach (var profile in profiles)
            {
                var profileQuery = CreateProfileQuery(profile);
                var cmd = profileQuery.BuildCommand(DbOperation.Insert);
                _sqlManager.ExecuteCommand(cmd);
            }
        }

        private ProfilesQuery CreateProfileQuery(Profile profile)
        {
            var profileQuery = new ProfilesQuery();
            profileQuery.AddChange(ProfileColumns.Company, profile.CompanyName);
            profileQuery.AddChange(ProfileColumns.Fullname, profile.ContactPerson);
            profileQuery.AddChange(ProfileColumns.Email, profile.Email);
            profileQuery.AddChange(ProfileColumns.WebPage, profile.WebPage);
            profileQuery.AddChange(ProfileColumns.Phone, profile.Phone);
            profileQuery.AddChange(ProfileColumns.Address, profile.GetAddressLine());
            profileQuery.AddChange(ProfileColumns.Description, profile.Description);
            profileQuery.AddChange(ProfileColumns.Gender, profile.Gender.ToString());
            profileQuery.AddChange(ProfileColumns.Branch, profile.Branch);
            profileQuery.AddChange(ProfileColumns.Certificates, profile.Certificates);
            profileQuery.AddChange(ProfileColumns.MotherCompany, profile.MotherCompany);
            profileQuery.AddChange(ProfileColumns.DaughterCompanies, profile.DaughterCompanies);
            profileQuery.AddChange(ProfileColumns.NumberOfOffices, profile.NumberOfOffices);
            profileQuery.AddChange(ProfileColumns.NumberOfEmployees, profile.NumberOfEmployes);
            profileQuery.AddChange(ProfileColumns.ExtraServices, profile.ExtraServices);
            profileQuery.AddChange(ProfileColumns.CountryOfOrigin, profile.CountryOfOrigin);
            profileQuery.AddChange(ProfileColumns.TargetGroup, profile.TargetGroup);
            profileQuery.AddChange(ProfileColumns.Source, profile.Source);
            return profileQuery;
        }

        public void DeleteProfiles()
        {
            var profileQuery = new ProfilesQuery();
            var cmd = profileQuery.BuildCommand(DbOperation.Delete);
            _sqlManager.ExecuteCommand(cmd);
        }

        public void UpdateProfiles(List<Profile> profiles)
        {
            foreach (var profile in profiles)
            {
                var profileQuery = CreateProfileQuery(profile);
                profileQuery.AddFilter(ProfileColumns.Id, Compare.Equal, profile.ProfileId);
                var cmd = profileQuery.BuildCommand(DbOperation.Update);
                _sqlManager.ExecuteCommand(cmd);
            }
        }

        #endregion

        #region Addresses

        public void CreateAddresses(List<Address> addresses)
        {
            foreach (var address in addresses)
            {
                address.Flag(FixedStatus.NotTouched);
                var addressesQuery = CreateAddressesQuery(address);
                var cmdCreateAddress = addressesQuery.BuildCommand(DbOperation.Insert);
                _sqlManager.ExecuteCommand(cmdCreateAddress);
                    
                var asssociateAddressWithPersonQuery = "update Profiles set addressId = @Address where ID = @ID";
                var cmdBindAddressToProfile = new SQLiteCommand(asssociateAddressWithPersonQuery);

                // Get created address ID
                var createdAddressFetchQuery = new AddressesQuery();
                createdAddressFetchQuery.AddFilter(AddressColumns.Guid, Compare.Equal, address.Guid);
                var cmd = createdAddressFetchQuery.BuildCommand(DbOperation.Select);
                var addressesTable = _sqlManager.GetTable(cmd);
                var createdAddress = Address.ConvertFromTable(addressesTable).FirstOrDefault();

                if (createdAddress != null)
                {
                    cmdBindAddressToProfile.Parameters.AddWithValue("@Address", createdAddress.Id);
                    cmdBindAddressToProfile.Parameters.AddWithValue("@ID", address.PersonId
                    );
                    _sqlManager.ExecuteCommand(cmdBindAddressToProfile);
                }
            }
        }

        public List<Address> GetAddresses(bool excludeFixed = false)
        {
            var addressesQuery = new AddressesQuery();
            if(excludeFixed)
                addressesQuery.AddFilter(AddressColumns.IsFixed, Compare.Equal, 0);
            var cmd = addressesQuery.BuildCommand(DbOperation.Select);
            var addressesTable = _sqlManager.GetTable(cmd);
            var addresses = Address.ConvertFromTable(addressesTable);
            return addresses;
        }

        private AddressesQuery CreateAddressesQuery(Address address)
        {
            var addressesQuery = new AddressesQuery();
            addressesQuery.AddChange(AddressColumns.CountryCode, address.CountryCode);
            addressesQuery.AddChange(AddressColumns.City, address.City);
            addressesQuery.AddChange(AddressColumns.Country, address.Country);
            addressesQuery.AddChange(AddressColumns.Street, address.Street);
            addressesQuery.AddChange(AddressColumns.PostalCode, address.PostalCode);
            addressesQuery.AddChange(AddressColumns.Guid, address.Guid);
            addressesQuery.AddChange(AddressColumns.IsFixed, address.IsFixed);
            return addressesQuery;
        }

        private AddressesQuery UpdateAddressQuery(Address address)
        {
            var addressesQuery = new AddressesQuery();
            if(address.CountryCode != null)
                addressesQuery.AddChange(AddressColumns.CountryCode, address.CountryCode);
            if (address.City != null)
                addressesQuery.AddChange(AddressColumns.City, address.City);
            if (address.Country != null)
                addressesQuery.AddChange(AddressColumns.Country, address.Country);
            if (address.Street != null)
                addressesQuery.AddChange(AddressColumns.Street, address.Street);
            if (address.PostalCode != null)
                addressesQuery.AddChange(AddressColumns.PostalCode, address.PostalCode);
            if (address.Guid != null)
                addressesQuery.AddChange(AddressColumns.Guid, address.Guid);
            return addressesQuery;
        }

        public void DeleteAddress()
        {
            var profileQuery = new AddressesQuery();
            var cmd = profileQuery.BuildCommand(DbOperation.Delete);
            _sqlManager.ExecuteCommand(cmd);
        }

        public void UpdateAddresses(List<Address> addresses)
        {
            foreach (var address in addresses)
            {
                var addressQuery = CreateAddressesQuery(address);
                addressQuery.AddFilter(AddressColumns.Id, Compare.Equal, address.Id);
                var cmd = addressQuery.BuildCommand(DbOperation.Update);
                _sqlManager.ExecuteCommand(cmd);
            }
        }

        public void UpdateAddressesIncludeEmpty(List<Address> addresses)
        {
            foreach (var address in addresses)
            {
                var addressQuery = UpdateAddressQuery(address);
                addressQuery.AddFilter(AddressColumns.Id, Compare.Equal, address.Id);
                var cmd = addressQuery.BuildCommand(DbOperation.Update);
                _sqlManager.ExecuteCommand(cmd);
            }
        }

        #endregion

        #region CountryPostalCodes

        public List<CountryPostalCode> GetCountryPostalCodes()
        {
            var countryPostalCodesQuery = new CountryPostalCodesQuery();
            var cmd = countryPostalCodesQuery.BuildCommand(DbOperation.Select);
            var countryPostalCodesTable = _sqlManager.GetTable(cmd);
            var countryPostalCodes = CountryPostalCode.ConvertFromTable(countryPostalCodesTable);
            return countryPostalCodes;
        }

        public Dictionary<string, string> GetCountryPostalCodesMap()
        {
            var countryPostalCodes = GetCountryPostalCodes();
            var countryPostalCodesMap = new Dictionary<string, string>();

            foreach (var countryPostalCode in countryPostalCodes)
            {
                countryPostalCodesMap.Add(countryPostalCode.CountryCode, countryPostalCode.Country);
            }

            return countryPostalCodesMap;
        }

        public void CreateCountryPostalCodes(List<CountryPostalCode> countryPostalCodes)
        {
            foreach (var countryPostalCode in countryPostalCodes)
            {
                var countryPostalCodeQuery = CreateCountryPostalCodeQuery(countryPostalCode);
                var cmd = countryPostalCodeQuery.BuildCommand(DbOperation.Insert);
                _sqlManager.ExecuteCommand(cmd);
            }
        }

        private CountryPostalCodesQuery CreateCountryPostalCodeQuery(CountryPostalCode countryPostalCode)
        {
            var countryPostalCodesQuery = new CountryPostalCodesQuery();
            countryPostalCodesQuery.AddChange(CountryPostalCodeColumns.CountryCode, countryPostalCode.Country);
            countryPostalCodesQuery.AddChange(CountryPostalCodeColumns.Country, countryPostalCode.CountryCode);
            return countryPostalCodesQuery;
        }

        public void DeleteCountryPostalCodes()
        {
            var profileQuery = new CountryPostalCodesQuery();
            var cmd = profileQuery.BuildCommand(DbOperation.Delete);
            _sqlManager.ExecuteCommand(cmd);
        }

        public void UpdateCountryPostalCode(List<Profile> profiles)
        {
            throw new NotImplementedException();
        }
       
        #endregion




    }
}
