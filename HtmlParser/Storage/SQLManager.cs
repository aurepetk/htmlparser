﻿using System;
using System.Data;
using System.Data.SQLite;
using HtmlParser.ConfigurationHelper;

namespace HtmlParser.Storage
{
    public class SqlManager
    {
        private const string ConnectionStringFormat = @"Data Source={0}; Version=3; FailIfMissing=True; Foreign Keys=True;";
        private readonly string _connectionString;

        public SqlManager()
        {
            var appDir = AppDomain.CurrentDomain.BaseDirectory;
            var dbPath = appDir + "\\" + ConfigurationStore.Instance.DatabaseStore;
            _connectionString = string.Format(ConnectionStringFormat, dbPath);
        }

        public SqlManager(string dbSource)
        {
            _connectionString = string.Format(ConnectionStringFormat, dbSource);
        }

        public DataTable GetTable(SQLiteCommand command)
        {
            var table = new DataTable();
            try
            {
                using (var conn = new SQLiteConnection(_connectionString))
                {
                    command.Connection = conn;
                    conn.Open();
                    var reader = command.ExecuteReader();
                   
                    table.Load(reader);
                }
            }
            catch (Exception e)
            {
                var errors = table.GetErrors();
                var error = e.Message;
                throw new Exception("Failed executeing SQL:" + command.CommandText);
                
            }
            finally
            {
                command.Dispose();
            }

            return table;
        }
        public int ExecuteCommand(SQLiteCommand command)
        {
            int effectedRecords;
            var table = new DataTable();
            try
            {
                using (var conn = new SQLiteConnection(_connectionString))
                {
                    command.Connection = conn;
                    conn.Open();
                    effectedRecords = command.ExecuteNonQuery();
                }
            }
            catch (SQLiteException e)
            {
                var error = e.Message;
                throw new Exception("Failed executeing SQL:" + command.CommandText);
            }
            finally
            {
                command.Dispose();
            }

            return effectedRecords;
        }
    }
}
