﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HtmlParser.Storage
{
    public class CountryPostalCode
    {
        public string CountryCode { set; get; }
        public string Country { set; get; }
        public int Id { set; get; }

        public CountryPostalCode(string countryCode, string country)
        {
            CountryCode = countryCode;
            Country = country;
        }

        public CountryPostalCode(DataRow row)
        {
            CountryCode = row["CountryCode"].ToString();
            Country = row["Country"].ToString();
            Id = Convert.ToInt32(row["ID"]);
        }

        public static List<CountryPostalCode> ConvertFromTable(DataTable table)
        {
            var countryPostalCodes = new List<CountryPostalCode>();
            foreach (DataRow row in table.Rows)
            {
                var countryPostalCode = new CountryPostalCode(row);
                countryPostalCodes.Add(countryPostalCode);
            }

            return countryPostalCodes;
        }
    }
}