﻿namespace HtmlParser.Storage
{
    public enum Compare { Equal, NotEqual, Less, More, MoreEqual, LessEqual}
    public enum DbOperation { Insert, Select, Update, Delete}
}
