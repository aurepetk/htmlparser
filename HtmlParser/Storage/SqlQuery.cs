﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace HtmlParser.Storage
{
    public class SqlQuery
    {
        protected string FilterQuery = "";
        protected List<SQLiteParameter> Filters;
        protected List<SQLiteParameter> Changes;
        protected string Tag = "@";
        protected Dictionary<DbOperation, string> BaseQueries;

        public SQLiteCommand BuildCommand(DbOperation operation)
        {
            var baseQuery = BaseQueries[operation];
            baseQuery = BuildQuery(baseQuery, operation);

            var sqlCommand = new SQLiteCommand(baseQuery);
            foreach (var param in Filters)
            {
                sqlCommand.Parameters.Add(param);
            }

            foreach (var param in Changes)
            {
                sqlCommand.Parameters.Add(param);
            }

            return sqlCommand;
        }

        protected string BuildQuery(string baseQuery, DbOperation operation)
        {
            if (operation == DbOperation.Insert)
            {
                baseQuery = BuildInsertQuery(baseQuery);
                return baseQuery;
            }
            else if (operation == DbOperation.Update)
            {
                baseQuery = BuildUpdate(baseQuery);
                return baseQuery;
            }
            else if (operation == DbOperation.Select || operation == DbOperation.Delete)
            {
                return baseQuery + FilterQuery;
            }

            throw new NotImplementedException(" Not implemented yet.");
        }


        public string BuildInsertQuery(string baseQuery)
        {
            var columns = new List<string>();
            var values = new List<string>();
            foreach (var change in Changes)
            {
                columns.Add(change.ParameterName.Substring(1));
                values.Add(change.ParameterName);
            }

            var columnString = string.Join(",", columns);
            var valueString = string.Join(",", values);
            baseQuery = string.Format(baseQuery, columnString, valueString);
            return baseQuery;
        }

        public string BuildUpdate(string baseQuery)
        {
            foreach (var change in Changes)
            {
                baseQuery += string.Format(" {0} = {1},", change.ParameterName.Substring(1), change.ParameterName);
            }

            baseQuery = baseQuery.Substring(0, baseQuery.Length - 1);
            return baseQuery + " " + FilterQuery;
        }

        protected string MakeTagUnique(string tagName)
        {
            var allTags = new List<string>();
            allTags.AddRange(Filters.Select(filter => filter.ParameterName));
            allTags.AddRange(Changes.Select(change => change.ParameterName));

            int index = 0;
            string tmpTagName = tagName;
            while (allTags.Contains(tmpTagName))
            {
                tmpTagName = tagName + index;
                index++;
            }

            return tmpTagName;
        }

        protected bool IsWhereSet => FilterQuery.ToLower().Contains("where");

        public void AddFilter(string taggedName, object value)
        {
            var filter = new SQLiteParameter(taggedName, value);
            Filters.Add(filter);
        }

        protected string GetCompareLiteral(Compare cmp)
        {
            if (cmp == Compare.Equal)
                return "=";
            if (cmp == Compare.Less)
                return "<";
            if (cmp == Compare.LessEqual)
                return "<=";
            if (cmp == Compare.More)
                return ">";
            if (cmp == Compare.MoreEqual)
                return ">=";
            if (cmp == Compare.NotEqual)
                return "<>";
            throw new Exception("Not supported");
        }
    }
}