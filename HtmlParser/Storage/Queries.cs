﻿using System.Collections.Generic;
using System.Data.SQLite;

namespace HtmlParser.Storage
{
    public class ProfilesQuery:SqlQuery
    {

        public ProfilesQuery()
        { 
            Filters = new List<SQLiteParameter>();
            Changes = new List<SQLiteParameter>();
            BaseQueries = new Dictionary<DbOperation, string>();

            var baseQuery = @"select profiles.id as profileID, profiles.company, profiles.description, profiles.email, profiles.address, profiles.fullname, profiles.gender, profiles.phone, profiles.source, profiles.webpage, 
            addresses.street, addresses.countrycode, addresses.postalcode, addresses.city, addresses.country,
            profiles.Branch, profiles.Certificates, profiles.MotherCompany, profiles.DaughterCompanies, profiles.NumberOfOffices, profiles.NumberOfEmployees, profiles.ExtraServices, profiles.CountryOfOrigin, profiles.TargetGroup
            from profiles 
            left join addresses on profiles.addressId = addresses.id";
            BaseQueries.Add(DbOperation.Select, baseQuery);

            baseQuery = "DELETE FROM Profiles";
            BaseQueries.Add(DbOperation.Delete, baseQuery);

            baseQuery = "INSERT INTO Profiles ({0}) values({1})";
            BaseQueries.Add(DbOperation.Insert, baseQuery);

            baseQuery = "UPDATE Profiles SET";
            BaseQueries.Add(DbOperation.Update, baseQuery);
        }

        public void AddFilter(ProfileColumns column, Compare compare, object value)
        {
            if (!IsWhereSet)
            {
                FilterQuery += " WHERE ";
            } 
            else
            {
                FilterQuery += " AND ";
            }

            var cmp = GetCompareLiteral(compare);

            var taggedName = Tag + column;
            taggedName = MakeTagUnique(taggedName);
            FilterQuery += string.Format("{0} {1} {2}", column, cmp, taggedName);
            AddFilter(taggedName, value);
        }

        public void AddChange(ProfileColumns column, object value)
        {
            var taggedName = Tag + column;
            taggedName = MakeTagUnique(taggedName);
            var change = new SQLiteParameter(taggedName, value);
            Changes.Add(change);
        }

    }

    public class AddressesQuery : SqlQuery
    {

        public AddressesQuery()
        {
            Filters = new List<SQLiteParameter>();
            Changes = new List<SQLiteParameter>();
            BaseQueries = new Dictionary<DbOperation, string>();

            var baseQuery = "SELECT * from Addresses";
            BaseQueries.Add(DbOperation.Select, baseQuery);

            baseQuery = "DELETE FROM Addresses";
            BaseQueries.Add(DbOperation.Delete, baseQuery);

            baseQuery = "INSERT INTO Addresses ({0}) values({1})";
            BaseQueries.Add(DbOperation.Insert, baseQuery);

            baseQuery = "UPDATE Addresses SET";
            BaseQueries.Add(DbOperation.Update, baseQuery);
        }

        public void AddFilter(AddressColumns column, Compare compare, object value)
        {
            if (!IsWhereSet)
            {
                FilterQuery += " WHERE ";
            }
            else
            {
                FilterQuery += " AND ";
            }

            var cmp = GetCompareLiteral(compare);

            var taggedName = Tag + column;
            taggedName = MakeTagUnique(taggedName);
            FilterQuery += string.Format("{0} {1} {2}", column, cmp, taggedName);
            AddFilter(taggedName, value);
        }

        public void AddChange(AddressColumns column, object value)
        {
            var taggedName = Tag + column;
            taggedName = MakeTagUnique(taggedName);
            var change = new SQLiteParameter(taggedName, value);
            Changes.Add(change);
        }

    }

    public class CountryPostalCodesQuery : SqlQuery
    {

        public CountryPostalCodesQuery()
        {
            Filters = new List<SQLiteParameter>();
            Changes = new List<SQLiteParameter>();
            BaseQueries = new Dictionary<DbOperation, string>();

            var baseQuery = "SELECT * from CountryPostalCodes";
            BaseQueries.Add(DbOperation.Select, baseQuery);

            baseQuery = "DELETE FROM CountryPostalCodes";
            BaseQueries.Add(DbOperation.Delete, baseQuery);

            baseQuery = "INSERT INTO CountryPostalCodes ({0}) values({1})";
            BaseQueries.Add(DbOperation.Insert, baseQuery);

            baseQuery = "UPDATE CountryPostalCodes SET";
            BaseQueries.Add(DbOperation.Update, baseQuery);
        }

        public void AddFilter(CountryPostalCodeColumns column, Compare compare, object value)
        {
            if (!IsWhereSet)
            {
                FilterQuery += " WHERE ";
            }
            else
            {
                FilterQuery += " AND ";
            }

            var cmp = GetCompareLiteral(compare);

            var taggedName = Tag + column;
            taggedName = MakeTagUnique(taggedName);
            FilterQuery += string.Format("{0} {1} {2}", column, cmp, taggedName);
            AddFilter(taggedName, value);
        }

        public void AddChange(CountryPostalCodeColumns column, object value)
        {
            var taggedName = Tag + column;
            taggedName = MakeTagUnique(taggedName);
            var change = new SQLiteParameter(taggedName, value);
            Changes.Add(change);
        }

    }


}