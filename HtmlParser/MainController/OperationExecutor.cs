﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using CsvHelper;
using HtmlParser.Addresses;
using HtmlParser.ConfigurationHelper;
using HtmlParser.Entities;
using HtmlParser.HtmlParse;
using HtmlParser.Storage;
using HtmlParser.View;
using OpenQA.Selenium;


namespace HtmlParser.MainController
{
    class OperationExecutor
    {
        List<Profile> _profiles1;
        List<Profile> _profiles2;
        List<Profile> _profiles3;
        readonly List<Profile> _profilesFromWeb;
        private List<Profile> _profilesFromDb;
        private readonly DbStorage _storage;

        public OperationExecutor()
        {
            _profiles1 = new List<Profile>();
            _profiles2 = new List<Profile>();
            _profiles3 = new List<Profile>();
            _profilesFromWeb = new List<Profile>();
            _profilesFromDb = new List<Profile>();
            _storage = new DbStorage();
        }

    public void BuildOperation(int choice)
        {
            if (choice == 0)
            {
                DeleteProfilesFromDB();
            }
            if (choice == 00)
            {
                DeleteAddressesFromDB();
            }
            else if (choice == 1)
            {
                ReadAllWebPages();
            }
            else if (choice == 2)
            {
                ImportFromDBToCSV();
                return;
            }
            else if (choice == 3)
            {
                ReadSofguidePage();
            }
            else if (choice == 4)
            {
                ReadSoftwareseachPage();
            }
            else if (choice == 5)
            {
                ImportCountryCodes();
                return;
            }
            else if (choice == 6)
            {
                FixAllPagesAddresses();
                return;
            }
            else if (choice == 7)
            {
                _storage.DeleteCountryPostalCodes();
                return;
            }
            else if (choice == 8)
            {
                ExportProfilesExtraToCsv();
                return;
            }
            else if (choice == 9)
            {
                ReadSoftwaregidsPage();
            }
            else
            {
                return;
            }
            _profilesFromDb = GetProfilesFromDb();
            var profilesFull = ImportProfilesToDB();
            //FixAllPagesAddresses();
            ConsoleView.WaitForInputToContinue();
            ConsoleView.DisplayParsedEntities(_profilesFromWeb);
            ExportToCsv(profilesFull);
        }

        private void DeleteProfilesFromDB()
        {
            var storage = new DbStorage();
            storage.DeleteProfiles();
        }

        private void DeleteAddressesFromDB()
        {
            var storage = new DbStorage();
            storage.DeleteAddress();
        }

        private void FixAllPagesAddresses()
        {
            var addressFixerMap = new Dictionary<string, AddressessFixer>();
            var countryMap = _storage.GetCountryPostalCodesMap();
            var addressFixerSoftguide = new AddressFixerSoftguide(_storage, countryMap);
            var addressFixerSoftwaregids = new AddressFixerSoftwaregids(_storage, countryMap);

            var fixedAddresses = new List<Address>();

            addressFixerMap.Add("www.softguide.de", addressFixerSoftguide);
            addressFixerMap.Add("https://www.softwaregids.nl/#tabs2", addressFixerSoftwaregids);

            foreach (var addressessFixer in addressFixerMap)
            {
                var profilesToUpdateAddress = GetProfilesFromDb(addressessFixer.Key, true);
                foreach (var profile in profilesToUpdateAddress)
                {
                    var fixedAddress = addressessFixer.Value.FixAddress(profile);
                    if (fixedAddress != null)
                        fixedAddresses.Add(fixedAddress);
                }
            }
            _storage.CreateAddresses(fixedAddresses);
        }

        private List<Profile> ImportProfilesToDB()
        {
            var profilesFull = new List<Profile>();
            profilesFull.AddRange(_profilesFromDb);
            profilesFull.AddRange(_profilesFromWeb);

            var oldProfilesWithChanges = _profilesFromWeb.Intersect(_profilesFromDb, new Comparer()).ToList();
            _storage.UpdateProfiles(oldProfilesWithChanges);

            var newProfiles = _profilesFromWeb.Except(oldProfilesWithChanges, new Comparer()).ToList();
            _storage.CreateProfiles(newProfiles);

            return profilesFull;
        }

        private void ExportProfilesExtraToCsv()
        {
            var profilesExtra = GetProfilesFromDb();
            ExportToCsv(profilesExtra);
        }

        private void ImportCountryCodes()
        {
            var countryPostalCodes = ParseCountryPostalCodes();
            _storage.CreateCountryPostalCodes(countryPostalCodes);
        }

        private void ReadSoftwareseachPage()
        {
            var websiteParser = BuildParser2();
            _profiles2 = GetProfilesFromPage(websiteParser);
            _profilesFromWeb.AddRange(_profiles2);
        }

        private void ReadSofguidePage()
        {
            var websiteParser = BuildParser1();
            _profiles1 = GetProfilesFromPage(websiteParser);
            _profilesFromWeb.AddRange(_profiles1);
        }

        private void ReadSoftwaregidsPage()
        {
            var websiteParser = BuildParser3();
            _profiles3 = GetProfilesFromPage(websiteParser);
            _profilesFromWeb.AddRange(_profiles3);
        }

        private void ImportFromDBToCSV()
        {
            _profilesFromDb = GetProfilesFromDb();
            ExportToCsv<Profile>(_profilesFromDb);
        }

        private void ReadAllWebPages()
        {
            var websiteParser1 = BuildParser1();
            _profiles1 = GetProfilesFromPage(websiteParser1);

            var websiteParser2 = BuildParser2();
            _profiles2 = GetProfilesFromPage(websiteParser2);

            _profilesFromWeb.AddRange(_profiles1);
            _profilesFromWeb.AddRange(_profiles2);
        }

        public List<CountryPostalCode> ParseCountryPostalCodes()
        {
            var countryCodesParser = new CountryCodesParser("https://countrycode.org/");
            return countryCodesParser.ParseCountryPostalCodes();
        }

        public List<Profile> GetProfilesFromPage(WebParser webParser)
        {
            Console.WriteLine("Reading website..");
            var profiles = webParser.ParseAll();
            var uniqueprofiles = webParser.DeleteRepetitiveProfiles(profiles);
            Console.WriteLine("Summary: Successfully read a total {0} of {1} profiles",
                uniqueprofiles.Count, webParser.TotalParseTargets);
            return uniqueprofiles;
        }

        private static IWebDriver BuildDriver()
        {
            var driverFactory = new WebDriverFactory();
            var selectedDriver = ConfigurationStore.Instance.SelectedDriver;
            var driver = driverFactory.BuildDriver(selectedDriver);
            return driver;
        }

        public WebParser BuildParser1()
        {
            var driver = BuildDriver();
            var websiteParser = new WebParserSofguiDe(driver);

            return websiteParser;
        }

        public WebParser BuildParser2()
        {
            var driver = BuildDriver();
            var websiteParser = new WebParserSoftwareSearch(driver);

            return websiteParser;
        }

        public WebParser BuildParser3()
        {
            var driver = BuildDriver();
            var websiteParser = new WebParserSoftwareGids(driver);

            return websiteParser;
        }

        public void ExportToCsv<T>(List<T> profiles)
        {
            using (var streamWriter = new StreamWriter(ConfigurationStore.Instance.CsvFile, false))
            {
                var csv = new CsvWriter(streamWriter);
                csv.WriteRecords(profiles);
            }
        }

        public List<Profile> GetProfilesFromDb(string source = "", bool? isAddressKnown = null)
        {

            var sqlManager = new SqlManager();

            var profileQuery = new ProfilesQuery();

            if(!string.IsNullOrEmpty(source))
                profileQuery.AddFilter(ProfileColumns.Source, Compare.Equal, source);

            if (isAddressKnown != null)
            {
                var operation = isAddressKnown.Value ? Compare.NotEqual : Compare.Equal;
                profileQuery.AddFilter(ProfileColumns.Address, operation, "Unknown" );
            }

            var command = profileQuery.BuildCommand(DbOperation.Select);
            var table = sqlManager.GetTable(command);
            var profiles = new List<Profile>();
            foreach (DataRow row in table.Rows)
            {
                var profile = new Profile(row);
                profiles.Add(profile);
            }

            return profiles;
        }       
    }
}
