﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CsvHelper;
using HtmlParser.ConfigurationHelper;
using HtmlParser.Entities;

namespace HtmlParser.EntityExport
{
    public class CsvExporter
    {
        private readonly string _csvFile;
        public CsvExporter()
        {
            _csvFile = ConfigurationStore.Instance.CsvFile;
        }

        public CsvExporter(string csvFile)
        {
            _csvFile = csvFile;
        }

        public void Export(List<Profile> data)
        {
            using (var textWriter = new StreamWriter(_csvFile))
            {
                var csv = new CsvWriter(textWriter);
                csv.WriteRecords(data);
            }
        }
    }
}
