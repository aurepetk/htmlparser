﻿using System;
using System.Collections.Generic;
using HtmlParser.Entities;
using HtmlParser.Storage;

namespace HtmlParser.Addresses
{
    class AddressFixerSoftguide: AddressessFixer
    {
        public AddressFixerSoftguide(DbStorage dbStorage, Dictionary<string, string> countryCodesMap) : base(dbStorage, countryCodesMap)
        {
        }

        public override Address FixAddress(Profile profile)
        {
            try
            {
                if (profile.GetAddressLine() == Profile.UnknownValue) return null;

                _addressColumn = GetAddressColumn(profile);
                var _fixedProfileId = profile.ProfileId;

                var countryCode = GetCountryCode(_addressColumn);
                var country = _countryCodesMap[countryCode];

                var streetAndTheRest = _addressColumn.Split(new string[] { " " + countryCode + " - " }, StringSplitOptions.None);
                var street = streetAndTheRest[0];

                var theRest = streetAndTheRest[1];
                var partsOftheRest = theRest.Split(' ');
                var postalCode = partsOftheRest[0];

                theRest = theRest.Replace(postalCode, "");
                var theRest2 = theRest.Split(',');
                var city = theRest2[0];
                var address = new Address(street, countryCode, city, country, _fixedProfileId, postalCode);

                return address;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        protected string GetAddressColumn(Profile infoSource)
        {
            var address = infoSource.GetAddressLine();
            var unwantedElement1 = infoSource.ContactPerson;
            var unwantedElement2 = infoSource.CompanyName;

            _addressColumn = _addressColumn.Replace(unwantedElement1, "");
            _addressColumn = _addressColumn.Replace(unwantedElement2, "");
            return address;
        }

        protected string GetCountryCode(string rawAddressLine)
        {
            foreach (var countryCode in _countryCodesMap.Keys)
            {
                var searchFor = countryCode + " -";
                if (rawAddressLine.Contains(searchFor))
                    return countryCode;
            }

            throw new Exception("No country code found.");
        }
    }
}
