﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using HtmlParser.Storage;

namespace HtmlParser.Addresses
{
    public class CountryCodesParser
    {
        private readonly string _sourceUrl;
        private readonly HtmlDocument _loadedDoc;
        public CountryCodesParser(string url)
        {
            _sourceUrl = url;
            var htmlWeb = new HtmlWeb();
            _loadedDoc = htmlWeb.Load(_sourceUrl);
        }

        public List<CountryPostalCode> ParseCountryPostalCodes()
        {
            var countryLines = GetAllCountryLines();
            var countryPostalCodes = new List<CountryPostalCode>();
            foreach (var countryLine in countryLines)
            {
                var country = ParseCountry(countryLine);
                var countryCode = ParseCountryCode(countryLine);
                var countryPostalCode = new CountryPostalCode(country, countryCode);
                countryPostalCodes.Add(countryPostalCode);
            }

            return countryPostalCodes;
        }

        private string ParseCountry(HtmlNode countryLine)
        {
            try
            {
                var countryElement = countryLine.SelectSingleNode("./td[1]/a");

                var country = countryElement.InnerText;
                return country;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return "";

        }

        private string ParseCountryCode(HtmlNode countryLine)
        {
            try
            {
                var countryCodeElement = countryLine.SelectSingleNode("./td[3]");
                var countryCode = countryCodeElement.InnerText.Split(' ')[0];
                return countryCode;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return "";
        }

        private List<HtmlNode> GetAllCountryLines()
        {
            var countryLines = _loadedDoc.DocumentNode.SelectNodes("//body/div[3]/div[1]/table/tbody/tr");
            return countryLines.ToList();
        }

    }
}
