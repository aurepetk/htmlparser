﻿namespace HtmlParser.Addresses
{
    public class CountryPostalCode
    {
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public CountryPostalCode(string country, string countryCode)
        {
            Country = country;
            CountryCode = countryCode;
        }
    }
}