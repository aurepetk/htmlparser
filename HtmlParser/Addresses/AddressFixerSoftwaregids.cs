﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlParser.Entities;
using HtmlParser.HtmlParse.ProfileDetails;
using HtmlParser.Storage;

namespace HtmlParser.Addresses
{
    class AddressFixerSoftwaregids: AddressessFixer
    {
        public AddressFixerSoftwaregids(DbStorage dbStorage, Dictionary<string, string> countryCodesMap) : base(dbStorage, countryCodesMap)
        {
        }

        public override Address FixAddress(Profile profile)
        {
            try
            {
                if (profile.GetAddressLine() == Profile.UnknownValue) return null;

                _addressColumn = profile.GetAddressLine();
                var _fixedProfileId = profile.ProfileId;

                var addressParts = _addressColumn.Split(new string[] {ProfileParserSoftwaregids.Delimiter},
                    StringSplitOptions.None);

                var street = addressParts[0];
                var city = addressParts[2];

                var postalCode = new String(addressParts[1].Where(Char.IsNumber).ToArray());
                var countryCode = new String(addressParts[1].Where(Char.IsLetter).ToArray());

                string country;
                try
                {
                    country = _countryCodesMap[countryCode];
                }
                catch (Exception)
                {
                    country = Profile.UnknownValue;
                }
                
                var address = new Address(street, countryCode, city, country, _fixedProfileId, postalCode);

                return address;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
