﻿using System.Collections.Generic;
using HtmlParser.Entities;
using HtmlParser.Storage;

namespace HtmlParser.Addresses
{
    public abstract class AddressessFixer
    {
        protected string _addressColumn;
        protected DbStorage _dbStorage;

        protected readonly Dictionary<string, string> _countryCodesMap;

        protected AddressessFixer(DbStorage dbStorage, Dictionary<string, string> countryCodesMap)
        {
            this._dbStorage = dbStorage;
            this._countryCodesMap = countryCodesMap;
        }

        public abstract Address FixAddress(Profile profile);
    }
}
