﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HtmlParser.Storage
{
    public class Address
    {
        public const string UnknownValue = "Unknown";
        public string Street { set; get; }
        public string CountryCode { set; get; }
        public string City { set; get; }
        public string Country { set; get; }
        public int Id { set; get; }
        public string PersonId { set; get; }
        public string PostalCode { set; get; }
        public string Guid { get; set; }
        public int IsFixed { get; set; }

        public Address(string street, string countryCode, string city, string country, string personId, string postalCode)
        {
            Street = street;
            CountryCode = countryCode;
            City = city;
            Country = country;
            PersonId = personId;
            PostalCode = postalCode;
            Guid = System.Guid.NewGuid().ToString();
        }

        public Address(string countryCode, string country, string postalCode)
        {
            CountryCode = countryCode;
            Country = country;
            PostalCode = postalCode;
        }

        public Address(DataRow row)
        {
            Street = row["Street"].ToString();
            CountryCode = row["CountryCode"].ToString();
            City = row["City"].ToString();
            Country = row["Country"].ToString();
            PostalCode = row["PostalCode"].ToString();
            Id = Convert.ToInt32(row["ID"]);
        }

        public string Print()
        {
            return $"PostalCode: {PostalCode}, Country: {Country}, CountryCode: {CountryCode}";
        }

        public static List<Address> ConvertFromTable(DataTable table)
        {
            var addresses = new List<Address>();
            foreach (DataRow row in table.Rows)
            {
                var address = new Address(row);
                addresses.Add(address);
            }

            return addresses;
        }

        public void Flag(FixedStatus status)
        {
            IsFixed = (int)status;
        }

    }
}