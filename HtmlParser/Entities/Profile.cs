﻿using System.Collections.Generic;
using System.Data;
using HtmlParser.ConfigurationHelper;
using HtmlParser.Storage;


namespace HtmlParser.Entities
{
    public class Profile
    {
        public const string UnknownValue = "Unknown";
        public string ProfileId { set; get; }
        public int AddressId { set; get; }
        public string ContactPerson { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string WebPage { get; set; }
        public string Phone { get; set; }
        public Gender Gender { get; set; }
        public string Description { get; set; }

        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }

        public string Branch { get; set; }
        public string Certificates { get; set; }
        public string MotherCompany { get; set; }
        public string DaughterCompanies { get; set; }
        public string NumberOfOffices { get; set; }
        public string NumberOfEmployes { get; set; }
        public string ExtraServices { get; set; }
        public string CountryOfOrigin { get; set; }
        public string TargetGroup { get; set; }
        public string Source { get; set; }

        private readonly string _addressLine;

        public string GetAddressLine()
        {
            return _addressLine;
        }
        
        public Profile(string contact, string company, string email, string phone, Gender gender,
            string description, string webPage, string addressLine, string certificates, string motherCompany, string daughterCompanies,
            string numberOfOffices, string numberOfEmployes, string extraServices, string countryOfOrigin, string targetGroup)
        {
            ContactPerson = contact;
            CompanyName = company;
            Email = email;
            Phone = phone;
            Gender = gender;
            Description = description;
            WebPage = webPage;
            this._addressLine = addressLine;
            Certificates = certificates;
            MotherCompany = motherCompany;
            DaughterCompanies = daughterCompanies;
            NumberOfOffices = numberOfOffices;
            NumberOfEmployes = numberOfEmployes;
            ExtraServices = extraServices;
            CountryOfOrigin = countryOfOrigin;
            TargetGroup = targetGroup;
        }

        public void SetAddress(Address addressObj)
        {
            Street = addressObj.Street;
            Country = addressObj.Country;
            CountryCode = addressObj.CountryCode;
            PostalCode = addressObj.CountryCode;
            City = addressObj.City;
        }


        public Profile(DataRow row)
        {
            _addressLine = row["Address"].ToString();
            Street = row["Street"].ToString();
            Country = row["Country"].ToString();
            CountryCode = row["CountryCode"].ToString();
            PostalCode = row["PostalCode"].ToString();
            ContactPerson = row["Fullname"].ToString();
            CompanyName = row["Company"].ToString();
            Email = row["Email"].ToString();
            WebPage = row["WebPage"].ToString();
            Phone = row["Phone"].ToString();
            Description = row["Description"].ToString();
            City = row["City"].ToString();
            Gender = DetermineGender( row["Gender"].ToString());
            ProfileId = row["profileID"].ToString();

            Branch = row["Branch"].ToString();
            Certificates = row["Certificates"].ToString();
            MotherCompany = row["MotherCompany"].ToString();
            DaughterCompanies = row["DaughterCompanies"].ToString();
            NumberOfOffices = row["NumberOfOffices"].ToString();
            NumberOfEmployes = row["NumberOfEmployees"].ToString();
            ExtraServices = row["ExtraServices"].ToString();
            CountryOfOrigin = row["CountryOfOrigin"].ToString();
            TargetGroup = row["TargetGroup"].ToString();
            Source = row["Source"].ToString();
        }

        public static Gender DetermineGender(string gender)
        {
            if (string.CompareOrdinal(gender.ToLower(), "frau") == 0 || string.CompareOrdinal(gender.ToLower(), "female") == 0)
                return Gender.Female;
            if (string.CompareOrdinal(gender.ToLower(), "herr") == 0 || string.CompareOrdinal(gender.ToLower(), "male") == 0)
                return Gender.Male;
            return Gender.Unknown;
        }

        public bool IsCopmlete()
        {
            return  string.CompareOrdinal(UnknownValue, ContactPerson) != 0 &&
                    string.CompareOrdinal(UnknownValue, CompanyName) !=  0 &&
                    string.CompareOrdinal(UnknownValue, Email) != 0 &&
                    string.CompareOrdinal(UnknownValue, WebPage) != 0 &&
                    string.CompareOrdinal(UnknownValue, Phone) != 0 &&
                    string.CompareOrdinal(UnknownValue, _addressLine) != 0 &&
                    string.CompareOrdinal(UnknownValue, Description) != 0;
        }

        public bool HasMinimumInfo()
        {
            var isMinimum = true;
            if (ConfigurationStore.Instance.IsCompanyRequired)
                isMinimum = string.CompareOrdinal(UnknownValue, CompanyName) != 0;
            if (ConfigurationStore.Instance.IsContactPersonRequired)
                isMinimum = isMinimum && string.CompareOrdinal(UnknownValue, ContactPerson) != 0;
            if (ConfigurationStore.Instance.IsEmailRequired)
                isMinimum = isMinimum && string.CompareOrdinal(UnknownValue, Email) != 0;
            if (ConfigurationStore.Instance.IsWebPageRequired)
                isMinimum = isMinimum && string.CompareOrdinal(UnknownValue, WebPage) != 0;
            if (ConfigurationStore.Instance.IsPhoneRequired)
                isMinimum = isMinimum && string.CompareOrdinal(UnknownValue, Phone) != 0;
            if (ConfigurationStore.Instance.IsAddressRequired)
                isMinimum = isMinimum && string.CompareOrdinal(UnknownValue, _addressLine) != 0;
            if (ConfigurationStore.Instance.IsDescriptionRequired)
                isMinimum = isMinimum && string.CompareOrdinal(UnknownValue, Description) != 0;
            if (ConfigurationStore.Instance.IsGenderRequired)
                isMinimum = isMinimum && Gender.Unknown == Gender;
            return isMinimum;
        }

        public static List<Profile> ConvertFromTable(DataTable table)
        {
            var profiles = new List<Profile>();
            foreach (DataRow row in table.Rows)
            {
                var profile = new Profile(row);
                profiles.Add(profile);
            }

            return profiles;
        }
    }
}
