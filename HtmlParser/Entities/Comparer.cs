﻿using System.Collections.Generic;

namespace HtmlParser.Entities
{
    public class Comparer : IEqualityComparer<Profile>
    {
        public bool Equals(Profile x, Profile y)
        {
            return x.Email == y.Email;
        }

        public int GetHashCode(Profile obj)
        {
            return obj.Email.GetHashCode();
        }
    }
}
