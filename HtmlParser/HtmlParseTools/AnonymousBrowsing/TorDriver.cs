﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using HtmlParseTools.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace HtmlParseTools.AnonymousBrowsing
{
    public class TorDriver
    {
        public IWebDriver Driver { get; set; }
        public Process FireFoxProcess { get; set; }
        public WebDriverWait Wait { get; set; }
        public static Process ThorProcess { get; set; }
        public TorDriver()
        {
            var torBinaryPath = ConfigurationManager.AppSettings["TorPath"];
            Build(torBinaryPath);
        }

        public TorDriver(string torBinaryPath)
        {
            Build(torBinaryPath);
        }

        private void Build(string torBinaryPath)
        {
            CallNewTorSession();
            try
            {
                FireFoxProcess = new Process
                {
                    StartInfo =
                    {
                        FileName = torBinaryPath,
                        Arguments = "-n",
                        WindowStyle = ProcessWindowStyle.Maximized
                    }
                };
                FireFoxProcess.Start();

                var profile = new FirefoxOptions();
                profile.SetPreference("network.proxy.type", 1);
                profile.SetPreference("network.proxy.socks", "127.0.0.1");
                profile.SetPreference("network.proxy.socks_port", 9150);
                Driver = new FirefoxDriver(profile);
                Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            }
            catch (Exception)
            {
                Driver = null;
            }
        }

        public static void RefreshTorIdentity()
        {
            Socket server = null;
            try
            {
                IPEndPoint ip = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9151);
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.Connect(ip);
                server.Send(Encoding.ASCII.GetBytes("your_password"));
                byte[] data = new byte[1024];
                int receivedDataLength = server.Receive(data);
                string stringData = Encoding.ASCII.GetString(data, 0, receivedDataLength);
                server.Send(Encoding.ASCII.GetBytes("SIGNAL NEWNYM" + Environment.NewLine));
                data = new byte[1024];
                receivedDataLength = server.Receive(data);
                stringData = Encoding.ASCII.GetString(data, 0, receivedDataLength);
                if (!stringData.Contains("250"))
                {
                    Console.WriteLine("Unable to signal new user to server.");
                    server.Shutdown(SocketShutdown.Both);
                    server.Close();
                }
            }
            finally
            {
                server.Close();
            }
        }

        public static void CallNewTorSession()
        {
            ThorProcess?.Kill();
            var torBaseBath = ConfigurationManager.AppSettings["TorBasePath"];
            ThorProcess = Process.Start(torBaseBath);
            Thread.Sleep(5000);
        }

        public static void CleanUp()
        {
            ThorProcess?.KillSafe();
            var firefoxProcesses = FindFireFox();
            foreach (var firefoxProcess in firefoxProcesses)
            {
                firefoxProcess.KillSafe();
            }
        }

        public static Process[] FindFireFox()
        {
            //string fireFoxPath = ConfigurationManager.AppSettings["FireFoxPath2"];
            var fireFoxProcesses = Process.GetProcesses("firefox.exe");
            return fireFoxProcesses;
        }

    }
}
