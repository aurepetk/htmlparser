﻿using System;
using HtmlParseTools.AnonymousBrowsing;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace HtmlParser
{
    public class WebDriverFactory
    {

        public WebDriverFactory()
        {
        }

        public IWebDriver BuildDriver(string selectedDriver)
        {
            if (string.Equals(selectedDriver, WebDriver.Chrome.ToString()))
                return new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory);
            if (string.Equals(selectedDriver, WebDriver.Firefox.ToString()))
            {
                var driverProfile = new FirefoxProfile(AppDomain.CurrentDomain.BaseDirectory);
                var opt = new FirefoxOptions {Profile = driverProfile};
                return new FirefoxDriver(opt);
            }

            if (string.Equals(selectedDriver, WebDriver.Tor.ToString()))
            {
                return new TorDriver().Driver;
            }

            if (string.Equals(selectedDriver, WebDriver.Ie.ToString()))
                return new InternetExplorerDriver(AppDomain.CurrentDomain.BaseDirectory);
            throw new NotSupportedException("Driver: "+ selectedDriver + " not supported.");
        }
    }

}
