﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace HtmlParseTools.Extensions
{
    public static class WebElementExtension
    {
        public static void Clear(this IWebElement element)
        {
            element.SendKeys(Keys.Control + "a");
            element.SendKeys(Keys.Control);
        }
    }
}
