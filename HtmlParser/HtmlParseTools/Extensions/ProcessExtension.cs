﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlParseTools.Extensions
{
    public static class ProcessExtension
    {
        public static void KillSafe(this Process process)
        {
            try
            {
                if (process.HasExited) return;
                process.Kill();
            }
            catch (Exception)
            {
                //
            }
        }
    }
}
