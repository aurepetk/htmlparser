﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace HtmlParseTools.Extensions
{
    public static class WebDriverExtension
    {

        public static void CloseDriver(this IWebDriver driver)
        {
            try
            {
                driver.Quit();
            }
            catch
            {
                Console.WriteLine("All windows closed already.");
            }
        }

        public static IWebElement WaitUntilElementExists(this IWebDriver driver, By elementLocator, float maxWaitTime = 6)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(maxWaitTime));
                return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(elementLocator));
            }
            catch (NoSuchElementException)
            { 
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found in current context page.");
                return null;
            }
        }

        public static IWebElement WaitUntilElementVisible(this IWebDriver driver, By elementLocator, float maxWaitTime = 6)
        { 
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(maxWaitTime));
                   return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(elementLocator));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found.");
                return null;
            }
        }

        public static IWebElement WaitUntilElementClickable(this IWebDriver driver, By elementLocator, float maxWaitTime = 6)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(maxWaitTime));
                return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(elementLocator));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found in current context page.");
                return null;
            }
        }

        public static void JavaScriptClick(this IWebDriver driver, IWebElement element)
        {
            var executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public static void NavigateToNewTab(this IWebDriver driver, IWebElement node)
        {
            OpenInNewTab(driver, node);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }

        public static void NavigateToExistingTab(this IWebDriver driver, IWebElement node)
        {
            var detailsUrl = node.GetAttribute("href");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            driver.Url = detailsUrl;
        }

        public static void NavigateToTabOfIndex(this IWebDriver driver, int index)
        {
            int attempts = 0;
            int maxAttempts = 10;
            while (attempts < maxAttempts)
            {
                try
                {
                    driver.SwitchTo().Window(driver.WindowHandles[index]);
                    break;
                }
                catch
                {
                    maxAttempts++;
                    Thread.Sleep(100);
                }
            }
        }

        public static void OpenInNewTab(this IWebDriver driver, IWebElement element)
        {
            var actionOpenLinkInNewTab = new Actions(driver);
            actionOpenLinkInNewTab
                .MoveToElement(element)
                .KeyDown(Keys.Control)
                .Click(element)
                .KeyUp(Keys.Control)
                .Perform();
            Thread.Sleep(10);
        }

        /// <summary>
        /// Warning: This uses the current url to open google.. Then use that to open a new tab
        /// </summary>
        public static void OpenNewTab(this IWebDriver driver)
        {
            driver.Url = "https://www.google.com/";
            var initiatorXpath = "//*[@id=\"gbw\"]/div/div/div[1]/div[2]/a";
            var initiator = driver.WaitUntilElementClickable(By.XPath(initiatorXpath));
            OpenInNewTab(driver, initiator);
        }

        public static void TryNavigate(this IWebDriver driver, string url)
        {
            try
            {
                driver.Url = url;
            }
            catch (Exception)
            {
                throw new Exception("Window is closed.");
            }
        }

        public static bool IsDriverClosed(this IWebDriver driver)
        {
            return driver.WindowHandles.Count == 0;
        }

        public static bool IsMultipleTabs(this IWebDriver driver)
        {
            return driver.WindowHandles.Count > 1;
        }

        public static void NavigateToDetailsOfNode(this IWebDriver driver, IWebElement node, float secondsToWait = 0f)
        {
            if (IsMultipleTabs(driver))
            {
                NavigateToNewTab(driver, node);
            }
            else
            {
                NavigateToExistingTab(driver, node);
            }
            if (secondsToWait > 0)
                Thread.Sleep((int)(secondsToWait * 1000));
        }

        public static bool CheckForCaptcha(this FirefoxDriver driver)
        {
            try
            {
                var captchaXpath = "/html/body/div[1]/div[2]/div[1]/h1";
                var captchaText = driver.WaitUntilElementExists(By.XPath(captchaXpath), 1);
                if (captchaText.Text.ToLower().Contains("step"))
                    return true;
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }

    }

    
}
