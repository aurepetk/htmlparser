﻿using System;
using System.Collections.Generic;
using HtmlParser.Entities;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse
{
    public static class HtmlParseHelper
    {
        public static List<string> CollectHrefs(List<IWebElement> sourceElements)
        {
            List<string> hrefs = new List<string>();
            foreach (var element in sourceElements)
            {
                var href = element.GetAttribute("href");
                hrefs.Add(href);
            }

            return hrefs;
        }

        public static string GetElementTextByXpath(string xpath, IWebDriver driver)
        {
            string elementText;
            try
            {
                var node = driver.FindElement(By.XPath(xpath));
                elementText = node.Text;
            }
            catch (Exception)
            {
                elementText = Profile.UnknownValue;
            }
            return elementText;
        }
    }
}
