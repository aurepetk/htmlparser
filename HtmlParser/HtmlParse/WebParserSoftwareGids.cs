﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlParser.Entities;
using HtmlParser.HtmlParse.ProfileDetails;
using HtmlParseTools.Extensions;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse
{
    class WebParserSoftwareGids: WebParser
    {

        public WebParserSoftwareGids(IWebDriver driver) : base(driver)
        {
            SourcePageUrl = "https://www.softwaregids.nl/#tabs2";
            NeededSourcePageSelector = "/html/body/div[3]/div";
            ProfileParser = new ProfileParserSoftwaregids(driver);
        }

        private void ParseCategoryElements(List<Category> categories)
        {
            foreach (var category in categories)
            {
                if (Driver.IsDriverClosed()) break;
                var profilesSourceOfCurrentBranch = CollectProfilesSourceOfCatalogue(category.Source, category.Name);
                ProcessDetails(profilesSourceOfCurrentBranch, category.Name);
            }
        }

        private List<Category> CreateCategories(IReadOnlyCollection<IWebElement> catalogueElemnts)
        {
            var categories = new List<Category>();
            foreach (var catalogueElement in catalogueElemnts)
            {
                var source = catalogueElement.GetAttribute("href");
                var name = catalogueElement.Text;
                var category = new Category(name, source);
                categories.Add(category);
            }

            return categories;
        }

        private List<string> CollectProfilesSourceOfCatalogue(string hrefCatalogue, string branchName)
        {
            List<string> profilesSourceOfCurrentBranch = new List<string>();
            var pageIndex = 0;
            var offsetPerPage = 20;
            var pageEndingFormat = "&advanced = &offset = {0}";
            var wasPageLaodedSuccessfully = true;
            var isNextPageExist = true;
            do
            {
                var currentOffset = pageIndex * offsetPerPage;
                var pageEnding = string.Format(pageEndingFormat, currentOffset);
                try
                {
                    var profilesSourcePerPage = CollectProfilesSourceOfPage(hrefCatalogue + pageEnding);
                    if (profilesSourcePerPage == null || !profilesSourcePerPage.Any())
                    {
                        break;
                    }

                    var profilesSourceCountPerPage = profilesSourcePerPage.Count;
                    if (profilesSourceCountPerPage < offsetPerPage)
                    {
                        profilesSourceOfCurrentBranch.AddRange(profilesSourcePerPage);
                        isNextPageExist = false;
                    }
                    if (profilesSourceOfCurrentBranch.Any() && (profilesSourceOfCurrentBranch[profilesSourceOfCurrentBranch.Count - 1] ==
                                                      profilesSourcePerPage[profilesSourceCountPerPage - 1]))
                    {
                        isNextPageExist = false;
                    }
                    else
                    {
                        profilesSourceOfCurrentBranch.AddRange(profilesSourcePerPage);
                        pageIndex++;
                    }
                    
                }
                catch (Exception)
                {
                    wasPageLaodedSuccessfully = false;
                }
            } while (wasPageLaodedSuccessfully && isNextPageExist);

            return profilesSourceOfCurrentBranch;
        }

        private List<string> CollectProfilesSourceOfPage(string url)
        {
            Driver.TryNavigate(url);
            // wait for first element in list..
            Driver.WaitUntilElementExists(
                By.XPath("//*[@id=\"content\"]/div/div/div[2]/div/div[1]/div[2]/strong/span/a"));
                
            var detailsSource = GetDetailsSourceHrefs();
            return detailsSource;
        }


        private void ProcessDetails(List<string> profilesSource, string branch)
        {
            foreach (var source in profilesSource)
            {
                if (Driver.IsDriverClosed()) break;
                Driver.TryNavigate(source);
                var profile = ParseDetails();
                if (profile != null)
                {
                    profile.Branch = branch;
                    profile.Source = SourcePageUrl;
                    Profiles.Add(profile);
                }
            }
        }

        protected List<string> GetDetailsSourceHrefs()
        {
            var elements = Driver.FindElements(
                By.XPath("//*[@id=\"content\"]/div/div/div[2]/div/div[1]/div/strong/span/a"));
            var elementsList = elements.ToList();
            var elementsHrefs = HtmlParseHelper.CollectHrefs(elementsList);
            return elementsHrefs;
        }

        protected List<Category> GetCategories(List<IWebElement> categoryLettersNodes)
        {
            var categories = new List<Category>();
            var numberOfDiv = 1;
            foreach (var letterNode in categoryLettersNodes)
            {
                try
                {
                    Driver.JavaScriptClick(letterNode);
                    var xpath = "../../div[" +numberOfDiv+ "]/a";
                    var catalogueElementsOnClick = letterNode.FindElements(By.XPath(xpath));
                    categories.AddRange(CreateCategories(catalogueElementsOnClick));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                numberOfDiv++;
            }
            return categories;
        }

        public override List<Profile> ParseAll()
        {
            try
            {
                Driver.Url = SourcePageUrl;
                var categoryLetters = GetCategoryLetters();
                var categoryNodes = GetCategories(categoryLetters);
                ParseCategoryElements(categoryNodes);
            }
            catch (Exception)
            {
                Driver.CloseDriver();
            }

            return Profiles;
        }

        private List<IWebElement> GetCategoryLetters()
        {
            try
            {
                var catalogueLettersElements = Driver.FindElements(By.XPath("//*[@id=\"tabs2\"]/div/h3/span"));
                return catalogueLettersElements.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    public class Category
    {
        public string Name { set; get; }
        public string Source { set; get; }

        public Category(string name, string source)
        {
            Name = name;
            Source = source;
        }
    }
}