﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlParser.Entities;
using HtmlParser.HtmlParse.ProfileDetails;
using HtmlParseTools.Extensions;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse
{
    public class WebParserSoftwareSearch: WebParser
    {
        protected List<string> SourceCatalogueHrefs { set; get; }

        public WebParserSoftwareSearch(IWebDriver driver) : base(driver)
        {
            SourcePageUrl = "https://software-search.com";
            NeededSourcePageSelector = "menu-main-1";
            ProfileParser = new ProfileParserSoftwareSearch(Driver);
        }

        protected List<string> CollectUnwantedMenuHrefs(List<IWebElement> sourceElements)
        {
            var unwantedMenuElements = GetUnwantedElements(sourceElements);

            var hrefsUnwanted = HtmlParseHelper.CollectHrefs(unwantedMenuElements);

            return hrefsUnwanted;
        }

        public List<IWebElement> GetUnwantedElements(List<IWebElement> sourceElements)
        {
            var unwantedMenuElements = new List<IWebElement>();
            var unwantedUrls = new List<string>
            {
                "https://software-search.com/software-vergleich-2018/",
                "https://software-search.com/employeeselfservices/",
                "https://software-search.com/login/",
                "https://software-search.com/register/",
                "https://software-search.com/lostpassword/"
            };
            foreach (var sourceElement in sourceElements)
            {
                var href = sourceElement.GetAttribute("href");
                if (unwantedUrls.Contains(href))
                {
                    unwantedMenuElements.Add(sourceElement);
                }
            }

            return unwantedMenuElements;
        }

        protected List<IWebElement> GetCategoryElements()
        {
            var reachMenu = Driver.FindElement(By.Id("menu-main-1"));
            var elements = reachMenu.FindElements(By.XPath("./li/ul/li/a"));
            return elements.ToList();
        }

        protected void ProcessDetails(List<IWebElement> menuElements)
        {                  
            DeleteUnwantedMenuItems(menuElements);
            foreach (var href in SourceCatalogueHrefs)
            {
                if (Driver.IsDriverClosed())
                {
                    break;
                }

                ProcessDetailsOfCatalogue(href);
            }
        }

        public void DeleteUnwantedMenuItems(List<IWebElement> sourceElements)
        {
            SourceCatalogueHrefs = HtmlParseHelper.CollectHrefs(sourceElements);

            foreach (var unwantedhref in CollectUnwantedMenuHrefs(sourceElements))
            {
                SourceCatalogueHrefs.Remove(unwantedhref);
            }
        }

        private void ProcessDetailsOfCatalogue(string href)
        {
            Driver.TryNavigate(href);
            var wasPageLaodedSuccessfully = true;
            while (wasPageLaodedSuccessfully)
            {
                try
                {
                    Driver.WaitUntilElementExists(
                        By.XPath("//*[@id=\"sabai-embed-wordpress-shortcode-1\"]/div/div[4]/div[1]/span"));
                    var profileElements = GetProfileElements();
                    foreach (var element in profileElements)
                    {
                        ((ProfileParserSoftwareSearch)ProfileParser).SetCurrentElement(element);
                        var profile = ParseDetails();
                        if (profile != null)
                        {
                            profile.Source = SourcePageUrl;
                            Profiles.Add(profile);
                        }
                    }

                    NavigateToNextPage();
                }
                catch (Exception)
                {
                    wasPageLaodedSuccessfully = false;
                }
            }
        }

        private void NavigateToNextPage()
        {
            var forwardButton = GetForwardButton();
            var onclick = forwardButton.GetAttribute("onclick");
            if (onclick == null)
                throw new Exception("No button to use to go to another page...");
            else
                Driver.JavaScriptClick(forwardButton);
        }

        protected IWebElement GetForwardButton()
        {
            var pagingButtons = Driver.FindElements(By.XPath("//*[@id=\"sabai-embed-wordpress-shortcode-1\"]/div/div[4]/div[2]/div/a"));
            IWebElement forwardButton = null;
            foreach (var button in pagingButtons)
            {
                var buttonText = button.Text;
                if (buttonText == "»")
                {
                    forwardButton = button;
                }
            }
            return forwardButton;
        }

        protected List<IWebElement> GetProfileElements()
        {
            var profileElemets = new List<IWebElement>();

            try
            {
                var profileElemetsPerRow = Driver.FindElements(
                    By.XPath("//*[@id=\"sabai-embed-wordpress-shortcode-1\"]/div/div[3]/div/div"));
                profileElemets.AddRange(profileElemetsPerRow);
            }
            catch (Exception)
            {
                var profileElementsPerColumn =
                    Driver.FindElements(
                        By.XPath("//*[@id=\"sabai-embed-wordpress-shortcode-1\"]/div/div[3]/div"));
                profileElemets.AddRange(profileElementsPerColumn);
            }

            return profileElemets;
        }

        public override List<Profile> ParseAll()
        {
            try
            {
                Driver.Url = SourcePageUrl;
                var categoryNodes = GetCategoryElements();
                ProcessDetails(categoryNodes);
            }
            catch (Exception)
            {
                Driver.CloseDriver();
            }

            return Profiles;
        }
    }
}
