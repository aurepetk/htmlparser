﻿using System;
using HtmlParser.Entities;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse.ProfileDetails
{
    public class ProfileParserSoftwaregids : ProfileParser
    {
        public const string Delimiter = "--;";
        protected override string ParseDescription()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[2]/td",
                Driver);
        }

        protected override string ParseCompany()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[1]/td/b/span/a",
                Driver);
        }

        protected override string ParseContactPerson()
        {
            throw new Exception("No person defined.");
        }

        protected override Gender ParseGender()
        {
            throw new Exception("No gender defined.");
        }

        protected override string ParseCertificates()
        {
           return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[15]/td", Driver);
        }

        protected override string ParseTargetGroup()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[17]/td",
                Driver);
        }

        protected override string ParseCountryOfOrigin()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[20]/td[2]",
                Driver);
        }

        protected override string ParseMotherCompany()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[22]/td",
                Driver);
        }

        protected override string ParseDaughterCompanies()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[23]/td",
                Driver);
        }

        protected override string ParseNumberOfOffices()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[24]/td",
                Driver);
        }

        protected override string ParseNumberOfEmployees()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[25]/td",
                Driver);
        }

        protected override string ParseExtraServices()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[30]/td",
                Driver);
        }

        protected override string ParsePhone()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[8]/td/span",
                Driver);
        }

        protected override string ParseEmail()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[11]/td/a",
                Driver);
        }

        protected override string ParseWebPage()
        {
            return HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[10]/td/span/a",
                Driver);
        }

        protected override string ParseAddress()
        {
            var addressLine1 =  HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[4]/td/span",
                Driver);
            var addressLine2 = HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[5]/td/span[1]",
                Driver);
            var addressLine3 = HtmlParseHelper.GetElementTextByXpath("/html/body/div[1]/div/div/div/div[2]/div/div/table/tbody/tr[5]/td/span[2]",
                Driver);
            
            return addressLine1 + ProfileParserSoftwaregids.Delimiter + addressLine2 + ProfileParserSoftwaregids.Delimiter + addressLine3;
        }

        public ProfileParserSoftwaregids(IWebDriver driver) : base(driver)
        {
        }
    }
}
