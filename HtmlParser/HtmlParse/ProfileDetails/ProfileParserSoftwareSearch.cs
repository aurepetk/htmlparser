﻿using System;
using HtmlParser.Entities;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse.ProfileDetails
{
    public class ProfileParserSoftwareSearch: ProfileParser
    {
        private IWebElement _currentDetailsElement;

        public void SetCurrentElement(IWebElement webElement)
        {
            _currentDetailsElement = webElement;
        }

        protected override string ParseCompany()
        {
            var propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[1]/h2/a"));
            var company = propertyHolder.Text;
            if (!string.IsNullOrEmpty(company)) return company;
            propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[1]/h2/a"));
            company = propertyHolder.Text;
            return company;
        }

        protected override string ParseContactPerson()
        {
            throw new Exception("No person defined.");
        }

        protected override Gender ParseGender()
        {
            throw new Exception("No gender defined.");
        }

        protected override string ParseCertificates()
        {
            throw new Exception("No certificates defined.");
        }

        protected override string ParseTargetGroup()
        {
            throw new Exception("No target groups defined.");
        }

        protected override string ParseCountryOfOrigin()
        {
            throw new Exception("No country of origin defined.");
        }

        protected override string ParseMotherCompany()
        {
            throw new Exception("No mother company defined.");
        }

        protected override string ParseDaughterCompanies()
        {
            throw new Exception("No daughter companies defined.");
        }

        protected override string ParseNumberOfOffices()
        {
            throw new Exception("No number of offices defined.");
        }

        protected override string ParseNumberOfEmployees()
        {
            throw new Exception("No number of employees defined.");
        }

        protected override string ParseExtraServices()
        {
            throw new Exception("No extra services defined.");
        }

        protected override string ParsePhone()
        {
            var propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[3]/div[2]/div[1]/span[1]"));
            var propertyValue = propertyHolder.Text;
            if (!string.IsNullOrEmpty(propertyValue)) return propertyValue;
            propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[3]/div[2]/div[1]/span[1]"));
            propertyValue = propertyHolder.Text;
            return propertyValue;
        }

        protected override string ParseEmail()
        {
            var propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[3]/div[2]/div[2]/a"));
            var propertyValue = propertyHolder.Text;
            if (!string.IsNullOrEmpty(propertyValue)) return propertyValue;
            propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[3]/div[2]/div[2]/a"));
            propertyValue = propertyHolder.Text;
            return propertyValue;
        }

        protected override string ParseWebPage()
        {
            var propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[3]/div[2]/div[3]/a"));
            var propertyValue = propertyHolder.Text;
            if (!string.IsNullOrEmpty(propertyValue)) return propertyValue;
            propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[3]/div[2]/div[3]/a"));
            propertyValue = propertyHolder.Text;
            return propertyValue;
        }

        protected override string ParseAddress()
        {
            string temporaryAddress = "";
            string address = "";
            string propertyValue = "";
            var propertyHolders = _currentDetailsElement.FindElements(By.XPath("./div/div[2]/div[3]/div[1]/span"));
            foreach (var propertyHolder in propertyHolders)
            {
                address = propertyHolder.Text;
                propertyValue = temporaryAddress + address;
                temporaryAddress = propertyValue;
            }
            propertyHolders = _currentDetailsElement.FindElements(By.XPath("./div/div[2]/div[3]/div[1]/span"));
            foreach (var propertyHolder in propertyHolders)
            {
                address = propertyHolder.Text;
                propertyValue = temporaryAddress + address;
                temporaryAddress = propertyValue;
            }
            return propertyValue;
        }

        protected override string ParseDescription()
        {
            var propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[4]"));
            var propertyValue = propertyHolder.Text;
            if (!string.IsNullOrEmpty(propertyValue)) return propertyValue;
            propertyHolder = _currentDetailsElement.FindElement(By.XPath("./div/div[2]/div[4]"));
            propertyValue = propertyHolder.Text;
            return propertyValue;
        }

        public ProfileParserSoftwareSearch(IWebDriver driver) : base(driver)
        {
        }
    }
}
