﻿using HtmlParser.Addresses;
using HtmlParser.ConfigurationHelper;
using HtmlParser.Entities;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse.ProfileDetails
{
    public abstract class ProfileParser
    {
        protected bool AllowIncompleteProfiles;
        protected AddressessFixer AddressFixer;
        protected IWebDriver Driver;

        protected ProfileParser(IWebDriver driver)
        {
            AllowIncompleteProfiles = ConfigurationStore.Instance.AllowIncompleteProfiles;
            Driver = driver;
        }

        private string GetPhoneFromDetails()
        {

            string phone;
            try
            {
                phone = ParsePhone();
            }
            catch
            {
                phone = Profile.UnknownValue;
            }

            return phone;
        }

        private string GetEmailFromDetails()
        {
            string email;
            try
            {
                email = ParseEmail();
            }
            catch
            {
                email = Profile.UnknownValue;
            }

            return email;
        }

        private string GetWebPageFromDetails()
        {
            string webPage;
            try
            {
                webPage = ParseWebPage();
            }
            catch
            {
                webPage = Profile.UnknownValue;
            }

            return webPage;
        }

        private string GetAddressFromDetails()
        {
            string address;
            try
            {
                address = ParseAddress();
            }
            catch
            {
                address = Profile.UnknownValue;
            }

            return address;
        }

        private string GetDescriptionFromDetails()
        {
            string description;
            try
            {
                description = ParseDescription();
            }
            catch
            {
                description = Profile.UnknownValue;
            }

            return description;
        }

        private string GetCompanyFromDetails()
        {

            string companyName;
            try
            {
                companyName = ParseCompany();
            }
            catch
            {
                companyName = Profile.UnknownValue;
            }

            return companyName;

        }

        private string GetContactPersonFromDetails()
        {
            string contactPerson;
            try
            {
                contactPerson = ParseContactPerson();
            }
            catch
            {
                contactPerson = Profile.UnknownValue;
            }

            return contactPerson;
        }

        private Gender GetGenderFromDetails()
        {
            Gender gender;
            try
            {
                gender = ParseGender();
            }
            catch
            {
                gender = Gender.Unknown;
            }

            return gender;
        }

        private string GetCertificatesFromDetails()
        {
            string certificates;
            try
            {
                certificates = ParseCertificates();
            }
            catch
            {
                certificates = Profile.UnknownValue;
            }

            return certificates;
        }

        private string GetMotherCompanyFromDetails()
        {
            string motherCompany;
            try
            {
                motherCompany = ParseMotherCompany();
            }
            catch
            {
                motherCompany = Profile.UnknownValue;
            }

            return motherCompany;
        }

        private string GetDaughterCompaniesFromDetails()
        {
            string daughterCompanies;
            try
            {
                daughterCompanies = ParseDaughterCompanies();
            }
            catch
            {
                daughterCompanies = Profile.UnknownValue;
            }

            return daughterCompanies;
        }

        private string GetNumberOfOfficesFromDetails()
        {
            string numberOfOffices;
            try
            {
                numberOfOffices = ParseNumberOfOffices();
            }
            catch
            {
                numberOfOffices = Profile.UnknownValue;
            }

            return numberOfOffices;
        }

        private string GetNumberOfEmployeesFromDetails()
        {
            string numberOfEmployees;
            try
            {
                numberOfEmployees = ParseNumberOfEmployees();
            }
            catch
            {
                numberOfEmployees = Profile.UnknownValue;
            }

            return numberOfEmployees;
        }

        private string GetExtraServicesFromDetails()
        {
            string extraServices;
            try
            {
                extraServices = ParseExtraServices();
            }
            catch
            {
                extraServices = Profile.UnknownValue;
            }

            return extraServices;
        }

        private string GetCountryOfOriginFromDetails()
        {
            string countryOfOrigin;
            try
            {
                countryOfOrigin = ParseCountryOfOrigin();
            }
            catch
            {
                countryOfOrigin = Profile.UnknownValue;
            }

            return countryOfOrigin;
        }

        private string GetTargerGroupFromDetails()
        {
            string targerGroup;
            try
            {
                targerGroup = ParseTargetGroup();
            }
            catch
            {
                targerGroup = Profile.UnknownValue;
            }

            return targerGroup;
        }

        public Profile Parse()
        {
            var contactPerson = GetContactPersonFromDetails();
            var companyName = GetCompanyFromDetails();
            var email = GetEmailFromDetails();
            var address = GetAddressFromDetails();
            var webPage = GetWebPageFromDetails();
            var phone = GetPhoneFromDetails();
            var description = GetDescriptionFromDetails();
            var gender = GetGenderFromDetails();

            var certificates = GetCertificatesFromDetails();
            var motherCompany = GetMotherCompanyFromDetails();
            var daughterCompanies = GetDaughterCompaniesFromDetails();
            var numberOfOffices = GetNumberOfOfficesFromDetails();
            var numberOfEmployees = GetNumberOfEmployeesFromDetails();
            var extraServices = GetExtraServicesFromDetails();
            var countryOfOrigin = GetCountryOfOriginFromDetails();
            var targetGroup = GetTargerGroupFromDetails();
            var profile = new Profile(contactPerson, companyName, email, phone, gender, description, webPage, address, certificates, motherCompany, daughterCompanies,
                numberOfOffices, numberOfEmployees, extraServices, countryOfOrigin, targetGroup);
            if (profile.IsCopmlete())
            {
                return profile;
            }

            if (AllowIncompleteProfiles && profile.HasMinimumInfo())
            {
                return profile;
            }
            return null;
        }

        protected abstract string ParsePhone();
        protected abstract string ParseEmail();
        protected abstract string ParseWebPage();
        protected abstract string ParseAddress();
        protected abstract string ParseDescription();
        protected abstract string ParseCompany();
        protected abstract string ParseContactPerson();
        protected abstract Gender ParseGender();

        protected abstract string ParseCertificates();

        protected abstract string ParseTargetGroup();

        protected abstract string ParseCountryOfOrigin();

        protected abstract string ParseMotherCompany();

        protected abstract string ParseDaughterCompanies();

        protected abstract string ParseNumberOfOffices();

        protected abstract string ParseNumberOfEmployees();

        protected abstract string ParseExtraServices();

        /*
         Per company it would be great to have:

        Branch (so AGF)
        Adres (address)
        Plaats (this is a bit wrong because here is the postal code and place)
        Algemeen telefoonnummer (phone number)
        Homepage
        Algemeen e-mailadres (general email address)
        Certificaten (Certificates)
        Doelgroep (Target group, so companies or consumers)
        Land van herkomst (country of origin)
        Onderdeel van (mother company)
        Dochter ondernemingen (daughter companies)
        Vestigingen (number of offices)
        Aantal medewerkers (number of employees)
        Extra diensten (extra services, here it’s: “software op maat” which means “custom software development”)
         */
    }
}
