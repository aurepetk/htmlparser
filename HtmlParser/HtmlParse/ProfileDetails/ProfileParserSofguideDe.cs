﻿using System;
using System.Linq;
using HtmlParser.Entities;
using HtmlParseTools.Extensions;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse.ProfileDetails
{
    public class ProfileParserSofguideDe:ProfileParser
    {
        protected override string ParseDescription()
        {
            string description;
            try
            {
                var id = Driver.FindElement(By.Id("beschreibung"));
                var propertyHolderP1 = id.FindElement(By.XPath("./div[2]/p[1]"));
                var propertyHolderP2 = id.FindElement(By.XPath("./div[2]/p[2]"));
                var propertyHolderP3 = id.FindElement(By.XPath("./div[2]/p[3]"));
                string p1 = propertyHolderP1.Text;
                string p2 = propertyHolderP2.Text;
                string p3 = propertyHolderP3.Text;
                description = p1 + " " + p2 + " " + p3 + "...";
            }
            catch (Exception)
            {
                description = Profile.UnknownValue;
            }
            return description;
        }

        protected override string ParseCompany()
        {
            string company;
            try
            {
                var propertyHolder = Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[2]/span/a"));
                company = propertyHolder.Text;
            }
            catch (Exception)
            {
                company = Profile.UnknownValue;
            }
            return company;
        }

        protected override string ParseContactPerson()
        {
            string person;
            try
            {
                var propertyHolder = Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[4]/span"));
                person = propertyHolder.Text;
                var purePerson = person.Split();
                var genderStringLengh = purePerson[0].Length + 1;
                person = person.Substring(genderStringLengh);
            }
            catch (Exception)
            {
                person = Profile.UnknownValue;
            }
            return person;
        }

        protected override Gender ParseGender()
        {
            try
            {
                var propertyHolder =
                    Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[4]/span"));
                var person = propertyHolder.Text;
                var purePerson = person.Split();
                var genderString = purePerson[0].ToLower().Trim();
                if (string.Equals(genderString, "frau"))
                    return Gender.Female;
                else if (string.Equals(genderString, "herr"))
                    return Gender.Male;
                else
                    return Gender.Unknown;
            }
            catch
            {
                return Gender.Unknown;
            }

        }

        protected override string ParseCertificates()
        {
            throw new Exception("No certificates defined.");
        }

        protected override string ParseTargetGroup()
        {
            throw new Exception("No target groups defined.");
        }

        protected override string ParseCountryOfOrigin()
        {
            throw new Exception("No country of origin defined.");
        }

        protected override string ParseMotherCompany()
        {
            throw new Exception("No mother company defined.");
        }

        protected override string ParseDaughterCompanies()
        {
            throw new Exception("No daughter companies defined.");
        }

        protected override string ParseNumberOfOffices()
        {
            throw new Exception("No number of offices defined.");
        }

        protected override string ParseNumberOfEmployees()
        {
            throw new Exception("No number of employees defined.");
        }

        protected override string ParseExtraServices()
        {
            throw new Exception("No extra services defined.");
        }

        protected override string ParsePhone()
        {
            string phone;
            try
            {
                var propertyHolder =
                    Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[5]/span"));
                var propertyValue = propertyHolder.Text;
                var phonePure = propertyValue.Split(':');
                phone = phonePure.Length == 1 ? phonePure[0] : phonePure[1];

            }
            catch (Exception)
            {
                phone = Profile.UnknownValue;
            }
            return phone;
        }

        protected override string ParseEmail()
        {
            string email;
            try
            {
                var propertyHolder =
                    Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[6]/span/a"));
                var propertyValue = propertyHolder.GetAttribute("href");
                var scriptSeperator = '?';
                var emailToSeperator = ':';
                var emailSeperator = ',';
                var emailParts = propertyValue.Split(scriptSeperator);
                propertyValue = emailParts[0];
                emailParts = propertyValue.Split(emailToSeperator);
                propertyValue = emailParts.Length > 1 ? emailParts[1] : emailParts[0];
                emailParts = propertyValue.Split(emailSeperator);
                email = emailParts[0];
            }
            catch (Exception)
            {
                email = Profile.UnknownValue;
            }

            return email;
        }

        protected override string ParseWebPage()
        {
            string webPage;
            try
            {
                var propertyValue = Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[2]/div[7]/span/a"));
                webPage = propertyValue.Text;
            }
            catch (Exception)
            {
                webPage = Profile.UnknownValue;
            }
            Driver.SwitchTo().Window(Driver.WindowHandles.First());
            return webPage;
        }

        protected override string ParseAddress()
        {
            string address;
            try
            {
                var propertyHolder =
                Driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div[2]/span/a"));
                Driver.NavigateToDetailsOfNode(propertyHolder, 3);
                var propertyValue = Driver.WaitUntilElementExists(By.XPath("/html/body/div[3]/div[2]/div[2]/div[2]/div[1]/span"));
                address = propertyValue.Text.Replace("\r\n", " ");
            }
            catch (Exception)
            {
                address = Profile.UnknownValue;
            }
            return address;
        }

        public ProfileParserSofguideDe(IWebDriver driver) : base(driver)
        {
        }
    }
}
