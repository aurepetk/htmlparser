﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlParser.Entities;
using HtmlParser.HtmlParse.ProfileDetails;
using HtmlParseTools.Extensions;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse
{
    public class WebParserSofguiDe: WebParser
    {
        public WebParserSofguiDe(IWebDriver driver) : base(driver)
        {
            SourcePageUrl = "https://www.softguide.de/";
            NeededSourcePageSelector = "/html/body/div[3]/div";
            ProfileParser = new ProfileParserSofguideDe(driver);
        }

        protected void ProcessCategories(List<IWebElement> categoryNodes)
        {
            List<string> categoriesSource = CollectCatagoriesSource(categoryNodes);
            List<string> profilesSource = CollectProfilesSource(categoriesSource);
            ProcessDetails(profilesSource);
        }

        private List<string> CollectProfilesSource(List<string> sourceCatalogueHrefs)
        {
            var profilesSource = new List<string>();
            foreach (var catalogue in sourceCatalogueHrefs)
            {
                if (Driver.IsDriverClosed()) break;
                AddProfilesSourceOfCatalogue(catalogue, profilesSource);
            }

            return profilesSource;
        }

        private void AddProfilesSourceOfCatalogue(string hrefCatalogue, List<string> profilesSource)
        {
            var pageIndex = 1;
            var wasPageLaodedSuccessfully = true;
            do
            {
                try
                {
                    var profilesSourcePerPage = CollectProfilesSourceOfPage(hrefCatalogue + "/" + pageIndex);
                    if (profilesSourcePerPage == null || !profilesSourcePerPage.Any())
                    {
                        break;
                    }
                    profilesSource.AddRange(profilesSourcePerPage);
                    pageIndex++;
                }
                catch (Exception)
                {
                    wasPageLaodedSuccessfully = false;
                }
            } while (wasPageLaodedSuccessfully);
        }

        private List<string> CollectProfilesSourceOfPage(string url)
        {
            Driver.TryNavigate(url);
            Driver.WaitUntilElementExists(
                By.ClassName("hpTcContent"));
            var detailbuttons = GetDetailsButtonsHrefs();
            return detailbuttons;
        }


        private void ProcessDetails(List<string> profilesSource)
        {
            foreach (var source in profilesSource)
            {
                if (Driver.IsDriverClosed()) break;
                Driver.TryNavigate(source);
                var profile = ParseDetails();
                if (profile != null)
                {
                    profile.Source = SourcePageUrl;
                    Profiles.Add(profile);
                }
            }
        }

        protected List<string> GetDetailsButtonsHrefs()
        {
            var elements = Driver.FindElements(By.XPath("//a[@class='button primary']"));
            var elementsList = elements.ToList();
            var elementsHrefs = CollectCatagoriesSource(elementsList);
            return elementsHrefs;
        }

        protected List<IWebElement> GetEntitySourceHtmlElements()
        {
            try
            {
                var reachMenu = Driver.FindElements(By.XPath("/html/body/div[3]/div/div/a"));
                return reachMenu.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        protected List<string> CollectCatagoriesSource(List<IWebElement> sourceElements)
        {
            List<string> usedHrefs = new List<string>();
            foreach (var element in sourceElements)
            {
                var href = element.GetAttribute("hrefCatalogue");
                usedHrefs.Add(href);
            }

            return usedHrefs;
        }

        public override List<Profile> ParseAll()
        {
            try
            {
                Driver.Url = SourcePageUrl;
                var categoryNodes = GetEntitySourceHtmlElements();
                ProcessCategories(categoryNodes);
            }
            catch (Exception)
            {
                Driver.CloseDriver();
            }
            
            return Profiles;
        }
    }
}
