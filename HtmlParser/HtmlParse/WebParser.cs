﻿using System.Collections.Generic;
using System.Linq;
using HtmlParser.Entities;
using HtmlParser.HtmlParse.ProfileDetails;
using OpenQA.Selenium;

namespace HtmlParser.HtmlParse
{
    public abstract class  WebParser
    {
        // TODO: move to someplace where storage should be handled.
        public List<Profile> Profiles;
        public int TotalParseTargets;

        protected string NeededSourcePageSelector;
        protected string SourcePageUrl;
        protected ProfileParser ProfileParser;

        protected IWebDriver Driver;

        protected WebParser(IWebDriver driver)
        {
            var list = new List<Profile>();
            Profiles = list;
            Driver = driver;
        }

        public List<Profile> DeleteRepetitiveProfiles(List<Profile> profiles)
        {
            profiles = profiles.Distinct(new Comparer()).ToList();
            return profiles;
        }

        // TODO: check where is profile added to the profile list.
        protected Profile ParseDetails()
        {
            TotalParseTargets++;
            Profile profile = ProfileParser.Parse();
            return profile;
        }

        public abstract List<Profile> ParseAll();
        //protected abstract List<IWebElement> GetCategories();
        //protected abstract void ProcessCategories(List<IWebElement> source);
    }
}