﻿using System.Configuration;

namespace HtmlParser.ConfigurationHelper
{

    public class ConfigurationStore
    {
        public string SelectedDriver { get; }
        public string CsvFile { get; }
        public string DatabaseStore { get; }
        public bool AllowIncompleteProfiles { get; }
        public bool IsContactPersonRequired { get; }
        public bool IsPhoneRequired { get; }
        public bool IsCompanyRequired { get; }
        public bool IsEmailRequired { get; }
        public bool IsWebPageRequired { get; }
        public bool IsAddressRequired { get; }
        public bool IsDescriptionRequired { get; }
        public bool IsGenderRequired { get; }
        public bool ClearDbBeforeScrapingProfiles { get; }
        public string TorPath { set; get; }

        private static volatile ConfigurationStore _instance;
        private static readonly object SyncRoot = new object();

        private ConfigurationStore()
        {
            SelectedDriver = ConfigurationManager.AppSettings["Driver"];
            CsvFile = ConfigurationManager.AppSettings["CsvFile"];
            DatabaseStore = ConfigurationManager.AppSettings["DatabaseStore"];
            AllowIncompleteProfiles = ParseBool("AllowIncompleteProfiles");
            IsContactPersonRequired = ParseBool("ContactPerson");
            IsPhoneRequired = ParseBool("Phone");
            IsCompanyRequired = ParseBool("Company");
            IsEmailRequired = ParseBool("Email");
            IsWebPageRequired = ParseBool("WebPage");
            IsAddressRequired = ParseBool("Address");
            IsDescriptionRequired = ParseBool("Description");
            IsGenderRequired = ParseBool("Gender");
            ClearDbBeforeScrapingProfiles = ParseBool("ClearDBBeforeScrapingProfiles");
            TorPath = ConfigurationManager.AppSettings["TorPath"];
        }

        public static ConfigurationStore Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new ConfigurationStore();
                    }
                }

                return _instance;
            }
        }

        public static bool ParseBool(string key)
        {
            bool.TryParse(ConfigurationManager.AppSettings[key], out var value);
            return value;
        }
    }
}
