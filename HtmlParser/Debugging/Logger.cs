﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debugging
{
    public static class Logger
    {
        public static bool wasOpened = false;
        public static void Log(string text, string filename = @"C:\Logs\Info.txt")
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(filename, wasOpened))
            {
                file.WriteLine(text);
                wasOpened = true;
            }
        }
    }
}
