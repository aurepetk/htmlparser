﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using HtmlParser;
using HtmlParser.Entities;
using HtmlParser.EntityExport;
using HtmlParser.TimedDriver;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCsvExport()
        {
            var csvExporter = new CsvExporter(@"D:\tetsCsv.csv");
            var profile = new Profile
            {
                Gender = Gender.Female,
                CompanyName = "TestCompany",
                ContactPerson =  "Zimmer",
                Phone = "1567498+4"
            };

            var profile2 = new Profile
            {
                Gender = Gender.Female,
                CompanyName = "TestCompany",
                ContactPerson = "Zimmer",
                Phone = "1567498+4"
            };

            var profile3 = new Profile
            {
                Gender = Gender.Female,
                CompanyName = "TestCompany",
                ContactPerson = "Zimmer",
                Phone = "1567498+4"
            };

            var profiles = new List<Profile> {profile, profile2, profile3};

            csvExporter.Export(profiles);
        }

        [TestMethod]
        public void SoftwareSearchTest()
        {
            var driver = new ChromeDriver("C:\\Users\\AlmantasK\\Desktop\\HTMLParser\\HtmlParser\\bin\\Debug");
            var advancedDriver = new WebDriverAdvanced(driver);
            advancedDriver.OpenNewTabViaGoogle();
            Thread.Sleep(2000);
            driver.SwitchTo().Window(driver.WindowHandles.First());
            driver.Url = "https://software-search.com/";
            var menu = driver.FindElement(By.Id("menu-main"));
            var elements = menu.FindElements(By.TagName("a"));
            //var elements = menu.FindElements(By.XPath("//*[@id=\"menu-item-1008\"]/a"))
            Assert.AreEqual(true, elements.Any());
            var url = elements[0].GetAttribute("href");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            driver.Url = url;
        }

        public void Test()
        {

        }
    }
}
