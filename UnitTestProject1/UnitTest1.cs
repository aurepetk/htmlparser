﻿using System.Collections.Generic;
using HtmlParser;
using HtmlParser.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var sourcePageUrl = "https://software-search.com";
            var driverFactory = new WebDriverFactory();
            var driver = driverFactory.BuildDriver();
            driver.Url = sourcePageUrl;
        }

        public class Comparer : IEqualityComparer<Profile>
        {
            public bool Equals(Profile x, Profile y)
            {
                return x.Email == y.Email;
            }

            public int GetHashCode(Profile obj)
            {
                return obj.Email.GetHashCode();
            }
        }
    }
}


