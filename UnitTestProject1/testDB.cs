﻿using System.Collections.Generic;
using System.Data;
using HtmlParser.Entities;
using HtmlParser.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class TestDb
    {
        [TestMethod]
        public void TestDBSelect()
        {
            var profileQuery = new ProfilesQuery();
            profileQuery.AddFilter(ProfileColumns.Company, Compare.Equal, "TestComp2");
            profileQuery.AddFilter(ProfileColumns.Fullname, Compare.Equal, "Tester2");

            var cmd = profileQuery.BuildCommand(DbOperation.Select);
            var sqlManager = new SqlManager(@"C:\Users\AlmantasK\Desktop\HTMLParser\HtmlParser\bin\Debug\Contacts.db");
            var table = sqlManager.GetTable(cmd);
            var rows = table.Rows;
            var profiles = new List<Profile>();
            foreach (DataRow row in rows)
            {
                var profile = new Profile(row);
                profiles.Add(profile);
            }
            Assert.AreEqual(1, profiles.Count);
        }

        [TestMethod]
        public void TestDBUpdate()
        {
            var profileQuery = new ProfilesQuery();
            profileQuery.AddChange(ProfileColumns.Fullname, "MainTester");
            profileQuery.AddFilter(ProfileColumns.Fullname, Compare.Equal, "Tester1" );

            var cmd = profileQuery.BuildCommand(DbOperation.Update);
            var sqlManager = new SqlManager(@"C:\Users\AlmantasK\Desktop\HTMLParser\HtmlParser\bin\Debug\Contacts.db");
            var table = sqlManager.GetTable(cmd);
            var rows = table.Rows;
            var profiles = new List<Profile>();
            foreach (DataRow row in rows)
            {
                var profile = new Profile(row);
                profiles.Add(profile);
            }
            
        }

        [TestMethod]
        public void TestDBDelete()
        {
            var profileQuery = new ProfilesQuery();
            //profileQuery.AddFilter(ProfileColumns.Company, Compare.Equal, "TestComp1");

            var cmd = profileQuery.BuildCommand(DbOperation.Delete);
            var sqlManager = new SqlManager(@"C:\Users\AlmantasK\Desktop\HTMLParser\HtmlParser\bin\Debug\Contacts.db");
            var effectedRows = sqlManager.ExecuteCommand(cmd);
            Assert.AreEqual(1, effectedRows);
        }


        [TestMethod]
        public void TestDBInsert()
        {
            var profileQuery = new ProfilesQuery();
            profileQuery.AddChange(ProfileColumns.Company, "TestComp3");
            profileQuery.AddChange(ProfileColumns.Fullname, "Tester3");

            var cmd = profileQuery.BuildCommand(DbOperation.Insert);
            var sqlManager = new SqlManager(@"C:\Users\AlmantasK\Desktop\HTMLParser\HtmlParser\bin\Debug\Contacts.db");
            var effectedRows = sqlManager.ExecuteCommand(cmd);
            Assert.AreEqual(1, effectedRows);
        }

    }
}
